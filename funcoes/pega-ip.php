<?php
	
	//Função para capturar ip do usuário
	function CapturaIp() {
	    $enderecoIp = "";

	    if(isset($_SERVER['HTTP_CLIENT_IP'])){
	        $enderecoIp = $_SERVER['HTTP_CLIENT_IP'];
	    }else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
	        $enderecoIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }else if(isset($_SERVER['HTTP_X_FORWARDED'])){
	        $enderecoIp = $_SERVER['HTTP_X_FORWARDED'];
	    }else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
	        $enderecoIp = $_SERVER['HTTP_FORWARDED_FOR'];
	    }else if(isset($_SERVER['HTTP_FORWARDED'])){
	        $enderecoIp = $_SERVER['HTTP_FORWARDED'];
	    }else if(isset($_SERVER['REMOTE_ADDR'])){
	        $enderecoIp = $_SERVER['REMOTE_ADDR'];
	    }else{
	        $enderecoIp = 'Desconhecido';
	    }

	    return $enderecoIp;
	}

?>