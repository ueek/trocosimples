<?php
	function limitaCaracteres($tamanho, $string, $sufixo = "...", $recuo = false, $encode = 'UTF-8') {
		$string = trim(strip_tags($string));
		$tamanhoCrop = ($recuo) ? ($tamanho - strlen($sufixo)) : $tamanho;

	    if( strlen($string) > $tamanho ){
	        $string = trim(mb_substr($string, 0, $tamanhoCrop, $encode)) . $sufixo;
	    }else{
	        $string = trim(mb_substr($string, 0, $tamanhoCrop, $encode));
	 	}
	    return $string;
	}

?>