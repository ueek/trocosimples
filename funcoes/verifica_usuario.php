<?php

	//Inicia a sessão
	session_start();  


	//Arquivos externos
    include_once '../models/admin.php';
    include_once '../config/database.php';
	//Controle de Usuário
    include "funcoes/pega-ip.php";

	if(!empty($_SESSION['idGestor'])){
		$id    = $_SESSION['idGestor'];
	}

	if(!empty($_SESSION['emailGestor'])){
		$login = $_SESSION['emailGestor'];
	}

	if(!empty($_SESSION['senhaGestor'])){
		$senha = $_SESSION['senhaGestor'];
	}


	if(!empty($id) and !empty($login) and !empty($senha)){
		
		// Inicializar banco de dados
    	$database = new Database();
    	$db = $database->getConnection();

	    // Instanciar objeto
	    $admin = new Admin($db);

		 $stmt = $admin->readByUserAndPassword($login, $senha);

		if($stmt->rowCount() == 0){
			header("Location: login.php");
			exit;
		}

	}else{
		$_SESSION['idGestor']      = "";
	    $_SESSION['emailGestor']   = "";
	    $_SESSION['senhaGestor']   = "";

		header("Location: login.php");
		exit;
	}
?>
