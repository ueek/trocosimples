<?php
    include_once 'models/blog-categoria.php';
    include_once 'models/blog-post.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

	// Instanciar objetos

    $blogCategoria = new BlogCategoria($db);
	$blogCategorias = $blogCategoria->read("nome");

    $blogPost = new BlogPost($db);

    if (isset($_GET['categoria'])) {
		$blogPosts = $blogPost->readPostsWithMainPicture("", "AND categoria_ids LIKE '%".$_GET['categoria']."%'");
    }else{
		$blogPosts = $blogPost->readPostsWithMainPicture();
    }

    $countDados = $blogPosts->rowCount();

    //echo $countDados;
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-blog.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section class="publicacoes">
	<div class="conteudo">

		<div class="categorias-publicacoes">
			<h3>Categorias</h3>

			<?php 

				while ($row = $blogCategorias->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					//$urlImagem = "gestor/uploads/sistemas/".$imagem; 

					echo"
						<a href='/blog/categoria/$url'>
							<div class='categoria-publicacoes soft-hover'>$nome</div>
						</a>
					";
					
				}

			?>
			<br>
		</div>

		<!--<h1>Blog</h1>

		<p class="desc-publicacoes">
			A Troco Simples sabe que a falta de troco é apenas um dos desafios que você precisa superar todos os dias. Por isso, te apoia na construção de um negócio rentável e muito mais lucrativo.
		</p>-->

		<div class="itens-publicacoes">
			<?php


				if($countDados <> 0){
					$itensPagina = 10;
				    $inicio = calcula_inicio_limite_query($itensPagina);
					
	                
				    if (isset($_GET['categoria'])) {
						$blogPosts = $blogPost->readPostsWithMainPicture("LIMIT {$inicio}, {$itensPagina}", "AND categoria_ids LIKE '%".$_GET['categoria']."%'");
				    }else{
						$blogPosts = $blogPost->readPostsWithMainPicture("LIMIT {$inicio}, {$itensPagina}");
				    }

	                
	                while ($row = $blogPosts->fetch(PDO::FETCH_ASSOC)){
						extract($row);

						$urlImagem = "/gestor/uploads/blog-posts/".$imagem; 
						
						if (strlen($titulo) > 70) {
							$titulo = substr($titulo, 0, 70)."...";
						}

						$texto = htmlspecialchars_decode($texto);
						$textoTruncado = substr($texto, 0, 100)."...";

						echo"
							<div class='item-publicacoes'>
								<div class='img-publicacoes'  style='background: url($urlImagem); background-repeat: no-repeat; background-position: center; background-size: cover;'></div>

								<h2>$titulo</h2>

								<p>$textoTruncado</p>

								<a href='post/$url'>
									<span class='soft-hover'>Ver mais</span>
								</a>
							</div>
						";
						
					}

					if(isset($_GET['linha']) and !empty($_GET['linha'])){
						paginacao($countDados, $itensPagina, 5, "pag");
					}else{
						paginacao($countDados, $itensPagina, 5, "pag");
					}
				}else{
					echo "Nenhum post encontrado.";
				}




				

			?>
			
			<br>
		</div>
	</div>
</section>

<section class="newsletter">
	<div class="conteudo">
		<h1>Newsletter</h1>

		<p>Quer receber nossas informações por e-mail? Então assine nossa Newsletter.</p>

		<form id="form-newsletter" class="form-newsletter">

			<div class="center-newsletter">
				<div class="campo-newsletter">
					<label>Nome*</label>
					<input class="soft-hover" type="text" name="nome">

					<br>
					<br>
				</div>

				<div class="campo-newsletter">
					<label>E-mail*:</label>
					<input class="soft-hover" type="text" name="email">				
				</div>
				<br>
			</div>

			<input class="soft-hover" type="submit" value="Enviar">

			<span class="">Campos marcados por um * são obrigatórios.</span>
		</form>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

		
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGBJ9W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->