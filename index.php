<?php
    include_once 'models/cliente.php';
    include_once 'config/database.php';
    include("funcoes/paginacao.php");
    
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

	// Instanciar objetos 

    $cliente = new Cliente($db);
	$myClientes = $cliente->read();
?>
<!doctype html>
<html lang="pt-br" class="no-js">
<head>
	<meta charset="UTF-8">
	
	<meta name="robots" content="index,follow">
	<meta name="author" content="Ueek Agência Digital">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />


	<link rel="shortcut icon" href="/img/icon.png">

	<link rel="stylesheet" href="/css/estilo.css?version=3"> <!-- ?version=11 -->
	<link rel="stylesheet" href="/css/paginas.css?version=3">
	<link rel="stylesheet" href="/css/mqueries.css?version=3">
	<link href="https://fonts.googleapis.com/css?family=Catamaran:300,400,500,600,700,800,900|Covered+By+Your+Grace|Doppio+One|Dosis:300,400,500,600,700,800|Exo+2:300,400,500,600,700,900|Istok+Web:400,400i,700,700i|Kalam:400,700|Lato:300,400,700,900|Montserrat:300,500,600,700,800,900|Open+Sans:300,400,600,700,800|Patrick+Hand|Playfair+Display:700,700i,900,900i|Poppins:400,500,600,700|Roboto:300,400,400i,500,700,900|Rubik:300,400,500,700,900|Signika:300,400,600,700|Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">


	<link rel="stylesheet" href="/lib/anjis/anicollection.css">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


	<?php
        //Title e Description SEO
        if(!isset($_GET['pg']) or empty($_GET['pg'])){ 

            echo "<title>TrocoSimples</title>";

            echo"
            <meta property='og:title' content='TrocoSimples'>
            <meta property='og:type' content='website'>
            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
            <meta property='og:site_name' content='TrocoSimples'>
            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

        }else{
            $pagina = $_GET['pg'];

            switch ($pagina) {
                case "home":
                    echo "<title>TrocoSimples</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "blog":
                    echo "<title>TrocoSimples - Blog</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Blog'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Blog'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "detalhes":
                    echo "<title>TrocoSimples - Detalhes</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Detalhes'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Detalhes'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "doacao":
                    echo "<title>TrocoSimples - Doação</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Doação'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Doação'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "fale-conosco":
                    echo "<title>TrocoSimples - Fale Conosco</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Fale Conosco'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Fale Conosco'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "imprensa":
                    echo "<title>TrocoSimples - Imprensa</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Imprensa'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Imprensa'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "nao-possui-revendedor":
                    echo "<title>TrocoSimples - Revendedor</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Revendedor'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Revendedor'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "para-empresas-saiba-mais":
                    echo "<title>TrocoSimples - Para Empresas</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Para Empresas'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Para Empresas'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "para-empresas":
                    echo "<title>TrocoSimples - Para Empresas</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Para Empresas'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Para Empresas'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "para-empresas-agradecimento":
                    echo "<title>TrocoSimples - Para Empresas</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Para Empresas'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Para Empresas'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "para-voce":
                    echo "<title>TrocoSimples - Para Você</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Para Você'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Para Você'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "post":
                    echo "<title>TrocoSimples - Post</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Post'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Post'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "revendedores":
                    echo "<title>TrocoSimples - Revendedores</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Revendedores'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Revendedores'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "seja-revendedor":
                    echo "<title>TrocoSimples - Seja Revendedor</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Seja Revendedor'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Seja Revendedor'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "sistemas":
                    echo "<title>TrocoSimples - Sistemas</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Sistemas'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Sistemas'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "sobre":
                    echo "<title>TrocoSimples - Sobre Nós</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Sobre Nós'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Sobre Nós'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;

                case "campanhas":
                    echo "<title>TrocoSimples - Campanhas</title>";

		            echo"
		            <meta property='og:title' content='TrocoSimples - Campanhas'>
		            <meta property='og:type' content='website'>
		            <meta property='og:image' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta property='og:site_name' content='TrocoSimples - Campanhas'>
		            <meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>
		            <meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>";

                    break;
            }

        }

    ?>

    <!-- Menu do celular -->
	<link rel="stylesheet" href="/lib/menu-ueek/css/estilo.css">
	<!-- Menu do celular -->

	<link rel="stylesheet" type="text/css" href="/lib/slick/slick.css">
  	<link rel="stylesheet" type="text/css" href="/lib/slick/slick-theme.css">


  	<?php
  		if($_GET['pg'] == "blog"){
  			echo "
  				<!-- Google Tag Manager -->
				<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','GTM-5QGBJ9W');</script>
				<!-- End Google Tag Manager -->
  			";
  		}elseif($_GET['pg'] == "campanhas"){
  			echo "
  				<!-- Google Tag Manager -->
				<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','GTM-P3VDDLJ');</script>
				<!-- End Google Tag Manager -->
  			";
  		}else{
  			echo "
  				<!-- Google Tag Manager -->
				<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','GTM-5BK8NR8');</script>
				<!-- End Google Tag Manager -->
  			";
  		}
  	?>

</head>

<body>

	<header class="topo header-fixo">

		<div class="conteudo">
			<a href="/">
				<img class="logo-header" src="/img/logo.png" alt="TrocoSimples">
			</a>

			<div id="redes-topo">
				<a href="https://www.facebook.com/trocosimples/" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-facebook-square"></i>
					</div>
				</a>
				<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-instagram"></i>
					</div>
				</a>
				<a href="https://www.linkedin.com/company/trocosimples.com.br/?viewAsMember=true" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-linkedin"></i>
					</div>
				</a>
				<a href="https://www.youtube.com/channel/UCLjhKFtnLt6nLcD34e81Vnw" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-youtube"></i>
					</div>
				</a>
				<a href="https://twitter.com/trocosimples" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-twitter-square"></i>
					</div>
				</a>
			</div>

			<nav class="menu">
				<ul>
					<!--<li class="li-principal"><a class="soft-hover" href="/">Home</a></li>-->

					<li class="drop-menu">
						<a class="soft-hover item-menu" href="/sobre">Sobre Nós</a>

						<div class="dropdown">
							<a class="soft-hover" href="/sobre">
								<div class="item-drop">Institucional</div>							
							</a>
							<a class="soft-hover" href="/imprensa">
								<div class="item-drop">Imprensa</div>							
							</a>
						</div>
					</li>
					
					<li class="li-principal"><a class="soft-hover" href="/para-voce">Para Você</a></li>
					<li class="li-principal"><a class="soft-hover" href="/para-empresas">Para Empresas</a></li>
					<li class="li-principal"><a class="soft-hover" href="/blog">Blog</a></li>
					<li class="li-principal"><a class="soft-hover" href="/fale-conosco">Fale Conosco</a></li>
					<li class="li-principal"><a class="soft-hover" href="/doacao">Doação</a></li>

					<li class="drop-menu">
						<a class="soft-hover item-menu" href="#">Comercial</a>

						<div class="dropdown">
<!-- 							<a class="soft-hover" href="/revendedores">
								<div class="item-drop">Revendedores</div>							
							</a> -->
							<a class="soft-hover" href="/sistemas">
								<div class="item-drop">Sistemas</div>							
							</a>
						</div>
					</li>

					<li class="li-principal"><a class="soft-hover" href="/">Acessos</a></li>
					<li class="li-principal"><a class="soft-hover" href="/campanhas">Campanhas</a></li>
				</ul>
			</nav>
			<br>

		</div>
	</header>

    <header class="topo">

    	<!-- Colocar sempre fora da div 'conteudo' -->
		<nav role='navigation'>

		  	<div id="menuToggle">

		    	<input id="checkbox" type="checkbox" />
		    
		    	<span class="span-menu"></span>
		    	<span class="span-menu"></span>
		    	<span class="span-menu"></span>
		    
			    <ul id="menu-cel">

			    	<div class="topo-menu">
			    		<div class="conteudo">
					  		<img src="/img/logo.png" alt="TrocoSimples">
				  		</div>
				  	</div>
			    	<div class="conteudo">

					  	<a href="/"><li class="soft-hover">Home</li></a>
					  	<a href="/sobre"><li class="soft-hover">Sobre Nós</li></a>

					  	<a href="/sobre"><li class="subitem-cel soft-hover">Institucional</li></a>
					  	<a href="/imprensa"><li class="subitem-cel soft-hover">Imprensa</li></a>

					  	<a href="/para-voce"><li class="soft-hover">Para Você</li></a>
					  	<a href="/para-empresas"><li class="soft-hover">Para Empresas</li></a>
					  	<a href="/blog"><li class="soft-hover">Blog</li></a>
					  	<a href="/fale-conosco"><li class="soft-hover">Fale Conosco</li></a>
					  	<a href="/doacao"><li class="soft-hover">Doação</li></a>
					  	<a href="#"><li class="soft-hover">Comercial</li></a>

					  	<!-- <a href="/revendedores"><li class="subitem-cel soft-hover">Revendedores</li></a> -->
					  	<a href="/sistemas"><li class="subitem-cel soft-hover">Sistemas</li></a>

					  	<a href="/"><li class="soft-hover">Acessos</li></a>
					  	<a href="/campanhas"><li class="soft-hover">Campanhas</li></a>

					  	<div id="redes-menuueek">
							<a href="https://www.facebook.com/trocosimples/" target="_blank">
								<div class="rede-menuueek soft-hover">
									<i class="fab fa-facebook-square"></i>
								</div>
							</a>
							<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank">
								<div class="rede-menuueek soft-hover">
									<i class="fab fa-instagram"></i>
								</div>
							</a>
							<a href="https://www.linkedin.com/company/trocosimples.com.br/?viewAsMember=true" target="_blank">
								<div class="rede-menuueek soft-hover">
									<i class="fab fa-linkedin"></i>
								</div>
							</a>
							<a href="https://www.youtube.com/channel/UCLjhKFtnLt6nLcD34e81Vnw" target="_blank">
								<div class="rede-menuueek soft-hover">
									<i class="fab fa-youtube"></i>
								</div>
							</a>
							<a href="https://twitter.com/trocosimples" target="_blank">
								<div class="rede-menuueek soft-hover">
									<i class="fab fa-twitter-square"></i>
								</div>
							</a>
							<br>
						</div>

				  	</div>
			    </ul>
	  		</div>
		</nav>
		<!-- Colocar sempre fora da div 'conteudo' -->


		<div class="conteudo">
			<a href="/">
				<img class="logo-header" src="/img/logo.png" alt="TrocoSimples">
			</a>

			<div id="redes-topo">
				<a href="https://www.facebook.com/trocosimples/" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-facebook-square"></i>
					</div>
				</a>
				<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-instagram"></i>
					</div>
				</a>
				<a href="https://www.linkedin.com/company/trocosimples.com.br/?viewAsMember=true" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-linkedin"></i>
					</div>
				</a>
				<a href="https://www.youtube.com/channel/UCLjhKFtnLt6nLcD34e81Vnw" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-youtube"></i>
					</div>
				</a>
				<a href="https://twitter.com/trocosimples" target="_blank">
					<div class="rede-topo soft-hover">
						<i class="fab fa-twitter-square"></i>
					</div>
				</a>
			</div>

			<nav class="menu">
				<ul>
					<!-- <li class="li-principal"><a class="soft-hover" href="/">Home</a></li> -->

					<li class="drop-menu">
						<a class="soft-hover item-menu" href="/sobre">Sobre Nós</a>

						<div class="dropdown">
							<a class="soft-hover" href="/sobre">
								<div class="item-drop">Institucional</div>							
							</a>
							<a class="soft-hover" href="/imprensa">
								<div class="item-drop">Imprensa</div>							
							</a>
						</div>
					</li>
					
					<li class="li-principal"><a class="soft-hover" href="/para-voce">Para Você</a></li>
					<li class="li-principal"><a class="soft-hover" href="/para-empresas">Para Empresas</a></li>
					<li class="li-principal"><a class="soft-hover" href="/blog">Blog</a></li>
					<li class="li-principal"><a class="soft-hover" href="/fale-conosco">Fale Conosco</a></li>
					<li class="li-principal"><a class="soft-hover" href="/doacao">Doação</a></li>

					<li class="drop-menu">
						<a class="soft-hover item-menu" href="#">Comercial</a>

						<div class="dropdown">
<!-- 							<a class="soft-hover" href="/revendedores">
								<div class="item-drop">Revendedores</div>							
							</a> -->
							<a class="soft-hover" href="/sistemas">
								<div class="item-drop">Sistemas</div>							
							</a>
						</div>
					</li>

					<li class="li-principal"><a class="soft-hover" href="/">Acessos</a></li>
					<li class="li-principal"><a class="soft-hover" href="/campanhas">Campanhas</a></li>
				</ul>
			</nav>
			<br>

		</div>
	</header>
	<br class="br-header">

	<?php
		//Novo pg recebe
        if(!isset($_GET['pg']) or empty($_GET['pg'])){
            include("home.php");
        }else{
            if($_SERVER['REQUEST_METHOD'] == 'GET'){
                $link = "{$_GET['pg']}.php";
                if(file_exists($link)){
                    include($link);
                }else{
                    //include("home.php");
                    include("erro.php");
                }
            }else{
                header("HTTP/1.0 404 Not Found");
            }
        }
	?>

	<footer id="rodape">
		<div class="conteudo">
			<div id="left-rdp" class="">
				<a href="/">
					<img src="/img/logo-rdp.png" alt="TrocoSimples">
				</a>
				<br>

				<h1>Contate-nos</h1>

				<ul>
					<li><i class="far fa-envelope-open"></i>&nbsp; contato@trocosimples.com.br</li>
					<li><i class="fab fa-whatsapp"></i>&nbsp; (41)3514-4930</li>
				</ul>

				<div id="redes-rdp">
					<a href="https://www.facebook.com/trocosimples/" target="_blank">
						<div class="rede-rdp soft-hover">
							<i class="fab fa-facebook-square"></i>
						</div>
					</a>
					<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank">
						<div class="rede-rdp soft-hover">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
					<a href="https://www.linkedin.com/company/trocosimples.com.br/?viewAsMember=true" target="_blank">
						<div class="rede-rdp soft-hover">
							<i class="fab fa-linkedin"></i>
						</div>
					</a>
					<a href="https://www.youtube.com/channel/UCLjhKFtnLt6nLcD34e81Vnw" target="_blank">
						<div class="rede-rdp soft-hover">
							<i class="fab fa-youtube"></i>
						</div>
					</a>
					<a href="https://twitter.com/trocosimples" target="_blank">
						<div class="rede-rdp soft-hover">
							<i class="fab fa-twitter-square"></i>
						</div>
					</a>
					<br>
				</div>

				<span>Faça já o download!</span>

				<a target="_blank" href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8&utm_source=site&utm_medium=botao&utm_content=ios">
					<img class="lojas-rdp" src="/img/ios-rdp.png" alt="TrocoSimples">
				</a>
				<a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor&utm_source=site&utm_medium=botao&utm_content=android">
					<img class="lojas-rdp" src="/img/android-rdp.png" alt="TrocoSimples">
				</a>

			<br>
		</div>

			</div>
			<div id="right-rdp" class="">
				
				<nav id="menu-rdp">
					<h1>Links</h1>

					<ul id="col-1">
						<a href="/"><li class="soft-hover">Home</li></a>
						<a href="/sobre"><li class="soft-hover">Sobre Nós</li></a>
						<a href="/"><li class="soft-hover subitem-rdp">Institucional</li></a>
						<a href="/imprensa"><li class="soft-hover subitem-rdp">Imprensa</li></a>
						<a href="/para-voce"><li class="soft-hover">Para Você</li></a>
						<a href="/para-empresas"><li class="soft-hover">Para Empresas</li></a>
						<a href="/blog"><li class="soft-hover">Blog</li></a>
					</ul>
					<ul>
						<a href="/fale-conosco"><li class="soft-hover">Fale Conosco</li></a>
						<a href="/doacao"><li class="soft-hover">Doação</li></a>
						<a href="/"><li class="soft-hover">Comercial</li></a>
						<!-- <a href="/revendedores"><li class="soft-hover subitem-rdp">Revendedores</li></a> -->
						<a href="/sistemas"><li class="soft-hover subitem-rdp">Sistemas</li></a>
						<a href="/"><li class="soft-hover">Acessos</li></a>
						<a href="/campanhas"><li class="soft-hover">Campanhas</li></a>
					</ul>
				</nav>

			</div>
			<br>
		</div>
	</footer>

	<section id="direitos">
		<div class="conteudo">
			<span>Troco Simples | Todos os direitos reservados.</span>

			<a href="https://ueek.com.br" target="_blank">
				<img src="/img/ueek.png" alt="Ueek Agência Digital">
			</a>
		</div>
	</section>

	<script src="/js/jquery.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/contato.js"></script>
	<script src="/js/validacoes.js"></script>
	<script src="/js/revendedores.js"></script>

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" />
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>

	<!-- BIBLIOTECA QUE FAZ O EFEITO INICIAL -->
	<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

	<!-- Menu do celular -->
	<script src="/lib/menu-ueek/js/menu.js"></script>
	<!-- Menu do celular -->

	<script src="/lib/slick/slick.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>