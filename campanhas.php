<?php
    include_once 'models/campanha.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

	// Instanciar objetos

    $myCampanha = new Campanha($db);

	$campanhas = $myCampanha->read("id DESC");

    $countDados = $campanhas->rowCount();

    //echo $countDados;
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-campanhas.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section class="publicacoes campanhas">
	<div class="conteudo">

		<h1>Aceite agora a Troco Digital e ganhe benefícios exclusivos!</h1>

		<p class="desc-publicacoes">
			Confira o regulamento das campanhas ativas que você encontra nos parceiros da Troco Simples:
		</p>

		<div class="itens-publicacoes">
			<?php


				if($countDados <> 0){
					$itensPagina = 10;
				    $inicio = calcula_inicio_limite_query($itensPagina);
					

					$campanhas = $myCampanha->read("id DESC", "LIMIT {$inicio}, {$itensPagina}");
				    	                
	                while ($row = $campanhas->fetch(PDO::FETCH_ASSOC)){
						extract($row);

						$urlImagem = "/gestor/uploads/campanhas/".$imagem; 
						$tituloTruncado = substr($titulo, 0, 60)."...";
						$resumo = htmlspecialchars(strip_tags($resumo));
						$textoTruncado = substr($resumo, 0, 100)."...";

						echo"
							<div class='item-publicacoes'>
								<div class='img-publicacoes'  style='background: url($urlImagem); background-repeat: no-repeat; background-position: center; background-size: cover;'></div>

								<h2>$tituloTruncado</h2>

								<p>$textoTruncado</p>

								<a target='_blank' href='$url'>
									<span class='soft-hover'>Ver mais</span>
								</a>
							</div>
						";
						
					}

					if(isset($_GET['linha']) and !empty($_GET['linha'])){
						paginacao($countDados, $itensPagina, 5, "pag");
					}else{
						paginacao($countDados, $itensPagina, 5, "pag");
					}
				}else{
					echo "Nenhuma campanha encontrada.";
				}

			?>
			
			<br>
		</div>


		<div class="itens-campanhas" class="">
			<a target="_blank" href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8&utm_source=site&utm_medium=botao&utm_content=ios">
				<img src="/img/ios.png" alt="TrocoSimples">
			</a>
			<a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor&utm_source=site&utm_medium=botao&utm_content=android">
				<img src="/img/android.png" alt="TrocoSimples">
			</a>
			<br>
		</div>

		<p class="info-publicacoes">
			* Campanhas válidas para estabelecimentos participantes, por tempo limitado.
		</p>

		<a href="http://www.trocosimples.com.br/landing-usuario/">
			<div class="btn-maisbeneficios soft-hover">Mais benefícios</div>
		</a>

	</div>
</section>

<section class="newsletter">
	<div class="conteudo">
		<h1>Newsletter</h1>

		<p>Quer receber nossas informações por e-mail? Então assine nossa Newsletter.</p>

		<form class="form-newsletter">

			<div class="center-newsletter">
				<div class="campo-newsletter">
					<label>Nome*</label>
					<input class="soft-hover" type="text" name="nome">

					<br>
					<br>
					<div class="form-radio">
	                    <label>Escolha uma opção:</label>
	                    <div class="radio">
	                        <label>
	                            <input type="radio" name="vocee" id="optionsRadios1" value="1" checked=""> Para Empresas
	                        </label>
	                    </div>
	                    <div class="radio">
	                        <label>
	                            <input type="radio" name="vocee" id="optionsRadios2" value="2"> Para você
	                        </label>
	                    </div>
	                </div>
				</div>

				<div class="campo-newsletter">
					<label>E-mail*:</label>
					<input class="soft-hover" type="text" name="email">				
				</div>
				<br>
			</div>

			<input class="soft-hover" type="submit" value="Enviar">
		</form>

		<span class="">Campos marcados por um * são obrigatórios.</span>
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3VDDLJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->