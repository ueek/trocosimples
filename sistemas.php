<?php
    include_once 'models/sistema.php';

	// Instanciar objetos
	$sistema = new Sistema($db);

	$sistemasTEF = $sistema->read("id DESC", "", "AND tipo = 1");
	$sistemasFrenteCaixa = $sistema->read("id DESC", "", "AND tipo = 2");
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-revendedores.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="sistemas">
	<div class="conteudo">

		<h1>Sistemas</h1>

		<p>
			Confira os sistemas TEF e Automação Comercial - PDV estão integrados ao Troco Simples:
		</p>


		<h2>TEF</h2>
		
		<div class="itens-sistemas regular5">
			<?php 

				while ($row = $sistemasTEF->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					$urlImagem = "gestor/uploads/sistemas/".$imagem; 

					echo"
					    <div>
					      	<div class='item-sistemas' style='background: url($urlImagem); background-position: center; background-repeat: no-repeat; background-size: cover;'></div>
					    </div>
					";
					
				}

			?>

			<br>
		</div>

		
		<h2>Frente da Caixa (PDV)</h2>
		
		<div class="itens-sistemas2 regular5">

			<?php 
				// Contador para controle - Mostrar sistemas de 3 em 3
				$contSistemasColuna = 0;
				while ($row = $sistemasFrenteCaixa->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					$urlImagem = "gestor/uploads/sistemas/".$imagem; 

					// Montar o valor de uma coluna
					$coluna = $coluna.
						"
							<div>
						      	<div class='item-sistemas' style='background: url($urlImagem); background-position: center; background-repeat: no-repeat; background-size: cover;'></div>
						    </div>
					   ";

				   // Incrementar o contador a cada linha adicionada à coluna
				   $contSistemasColuna++; 

				   // Se a coluna já possui 3 linhas, escreve na tela e zera os controladores
				   if ($contSistemasColuna == 3) {
						echo"
							<div class='item-sistemas2'>
								$coluna
								<br>
							</div>
						";

						$contSistemasColuna = 0;
						$coluna = "";
				   }
				}

				// se ao final do while a variável coluna estiver com algum valor significa que é a última coluna teve menos de 3 itens, então deve ser escrita mesmo assim
				echo"
					<div class='item-sistemas2'>
						$coluna
						<br>
					</div>
				";

			?>
		</div>

		<!--<a href="/detalhes">
			<div id="btn-sistemas" class="soft-hover">Saiba Mais</div>
		</a>-->
		<br>

	</div>
</section>

<section class="formularios" id="formulario">
	<div class="conteudo">
		<h1>PARA EMPRESAS</h1>

		<h2>Saia do prejuízo pela falta de moedas</h2>

		<p>
			Rodamos integrados ao seu frente de caixa (PDV) e ou TEF,  para que você entregue troco digital direto no CPF dos seus clientes.
		</p>

		<form class="form-para-empresas formulario">
			<div class="col-1">
				
				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Responsável*</label>
				<input class="soft-hover" type="text" name="responsavel" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

			</div>
			<div class="col-2">
				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>Estado*</label>
				<input class="soft-hover" type="text" name="estado" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>

<section id="txt-sistemas">
	<div class="conteudo">
		Se você representa um sistema TEF e/ou Automação Comercial - PDV  e quer fazer parte da nossa rede de integração, preencha o formulário abaixo que o time Troco Simples entrará em contato.
	</div>
</section>

<section class="formularios">
	<div class="conteudo">
		<h2>Quero Integrar ao meu Sistema</h2><br>

		<form id="form-sistema" class="formulario">
			<div class="col-1">
				
				<label>Nome do Sistema*</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>Responsável*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">
			</div>
			<div class="col-2">


				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<div class="form-radio">
                    <label>Tipo de Sistema:</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="vocee" id="optionsRadios1" value="PDV" checked=""> Frente de Caixa (PDV)
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="vocee" id="optionsRadios2" value="TEF"> TEF
                        </label>
                    </div>
                </div>
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Vamos guardar os seus dados só enquanto quiser. Ficarão em segurança e a qualquer momento pode editá-los ou deixar de receber as nossas mensagens.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>


<!--<section class="formularios">
	<div class="conteudo">
		<h2>Sistemas</h2><br>

		<form class="formulario">
			<div class="col-1">
				
				<label>Nome*</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

				<label>Segmento*</label>
				<input class="soft-hover" type="text" name="segmento" placeholder="">

			</div>
			<div class="col-2">
				<label>Último Nome*</label>
				<input class="soft-hover" type="text" name="ultimonome" placeholder="">

				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Número de clientes*</label>
				<input class="soft-hover" type="text" name="numeroclientes" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>-->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->