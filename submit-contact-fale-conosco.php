<?php	

	try {
		include('Egoi/Factory.php');
		
		$apikey = "7d94856e2d2db073b69d630d235498fd7cd5caa0";

		//Dados do formulario
		$nomeDestinatario      	= $_POST['nome'];
		$emailDestinatario     	= $_POST['email'];

	    $arguments = array(
	        "apikey" => $apikey,
	        "listID" => 4,
	        "status" => 1,
	        "first_name" => $nomeDestinatario,
	        "from" => "ola@trocosimples.com.br",
	        "email" => $emailDestinatario
	    );

	    $api = EgoiApiFactory::getApi(Protocol::Rest);
	    $result = $api->addSubscriber($arguments);
	    

	    
		$retorno = array("status" => 1, "msg" => "Mensagem de contato enviada com sucesso! Em breve entraremos em contato.");
		echo json_encode($retorno);
		exit;	
	} catch (Exception $e) {
		$retorno = array("status" => 0, "msg" => "Erro ao enviar seus dados. Tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}


?>