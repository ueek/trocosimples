<!doctype html>
<html lang="pt-br" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="index,follow">
	<meta name="author" content="Ueek Agência Digital">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0">

	<title>Troco Simples Empresas - Aplicativo troco digital</title>
	<meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>

	<meta property='og:title' content='Troco Simples Empresas - Aplicativo troco digital'>
	<meta property='og:type' content='website'>
	<meta property='og:site_name' content='Troco Simples'>
	<meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>

	<link rel="shortcut icon" href="img/icon.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">

	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/mqueries.css">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KZBTX86');</script>
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZBTX86"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header id="topo">
		<img src="img/logo.png" alt="Troco Simples" class="efeito">
	</header>

	<section id="acabe" class="conteudo">
		<div id="txt" class="efeito">
			<h1>
				ACABE COM O PREJUÍZO
				<br>CAUSADO PELA FALTA DE
				<br>TROCO
			</h1>

			<p>
				Com troco digital você para de<br>
				arredondar valores e de perder<br>
				tempo procurando por moedas. 
			</p>

			<p>
				Integrado ao seu sistema de frente de<br>
				caixa (PDV) ou TEF, deposite o troco<br>
				digital direto no CPF dos seus clientes.
			</p>
		</div>

		<div id="formulario" class="efeito">
			<header id="topo-formulario">
				PREENCHA OS CAMPOS PARA FALAR COM<br>UM CONSULTOR:
			</header>

			<!--Paste this code where you want the form to appear-->
            <div id="a2ede1IMre4NhQhfgADe554d8c07"></div>
            
			<!--form>
				<input type="text">
				<input type="text">
				<input type="text">
				<input type="text">
				<input type="text">

				<input type="submit" value="Enviar">
			</form>

			<span>*Contato sem compromisso.</span-->
		</div>
	<br>
	</section>
	
	<section id="como-funciona" class="conteudo">
		<div id="imagem" class="efeito"></div>

		<div id="txt" class="efeito">
			<h1>Como funciona?</h1>

			<p>
				A Troco Simples digitaliza o troco de sua rede de parceiros para que você não perca mais tempo, dinheiro e clientes. 
			</p>

			<p>
				O troco digital é a solução mais fácil, barata e segura para acabar com o problema da falta de moedas e pequenas notas, e facilitar suas transações financeiras que envolvem dinheiro em espécie.
			</p>
		</div>
	<br>
	</section>
	
	<section id="beneficios">
		<div class="conteudo">
			<h1>MENOS PREJUÍZO, MAIS LUCROS</h1>

			<div class="coluna efeito">
				<div class="item" id="elimina">
					Elimina a quebra 
					<br>de caixa
				</div>

				<div class="item" id="diminui">
					Diminui o fundo de 
					<br>caixa
				</div>
			</div>

			<div class="coluna efeito">
				<div class="item" id="acabe">
					Acaba com a perda de tempo<br>
					na busca por moedas
				</div>

				<div class="item" id="reduz">
					Reduz custos operacionais<br>
					para aquisição de troco
				</div>
			</div>
		<br>
		</div>
	</section>

	<section id="conhecer" class="conteudo efeito">
		<button>Quero Conhecer</button>
		<br><br>
		A solução para a falta de troco já existe, 
		<br>acesse para saber mais: 
		
		<br>
		<br>www.trocosimples.com.br
	</section>

	<footer id="rodape">
		<img src="img/logo.png" alt="Troco Simples" id="marca" class="efeito">
		<br>contato@trocosimples.com.br | (41) 3514 4930
		<ul>
			<li>
				<a href="https://www.facebook.com/trocosimples" target="_blank" class="soft-hover">
					<img src="img/fb.png" class="rede"> 
					<span>/trocosimples</span>
				</a>
			</li>
			<li>
				<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank" class="soft-hover">
					<img src="img/ig.png" class="rede">
					<span>@trocosimplesoficial</span>
				</a>
			</li>
		</ul>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

	<script src="js/main.js" type="text/javascript"></script>

	<!--Paste this code on the bottom of your page-->
	<script type="text/javascript" id="script_2ede1IMre4NhQhfgADe554d8c07">
	var _egoiLoaderMulti = _egoiLoaderMulti || [];
	var _egoiLoaderValues = [];
	_egoiLoaderValues.push(["_setForm", "2ede1IMre4NhQhfgADe554d8c07"]);
	_egoiLoaderValues.push(["_setResource", "14252337bee3af776dfb85b3575f4082"]);
	_egoiLoaderValues.push(["_setUrl", "https://28.e-goi.com/"]);
	_egoiLoaderValues.push(["_setType", "normal"]);
	_egoiLoaderValues.push(["_setWidth", "650"]);
	_egoiLoaderValues.push(["_setHeight", "746"]);
	_egoiLoaderMulti.push(_egoiLoaderValues);
	(function() {
	var egoi = document.createElement("script"); egoi.type = "text/javascript"; egoi.async = true;
	egoi.src = "https://28.e-goi.com/include/javascript/egoi.js";
	var e = document.getElementById("script_2ede1IMre4NhQhfgADe554d8c07"); e.parentNode.insertBefore(egoi, e);
	})();
	</script>
</body>
</html>