<!doctype html>
<html lang="pt-br" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="index,follow">
	<meta name="author" content="Ueek Agência Digital">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0">

	<title>Troco Simples Empresas - Aplicativo troco digital</title>
	<meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>

	<meta property='og:title' content='Troco Simples Empresas - Aplicativo troco digital'>
	<meta property='og:type' content='website'>
	<meta property='og:site_name' content='Troco Simples'>
	<meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>

	<link rel="shortcut icon" href="img/icon.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">

	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/mqueries.css">
</head>

<body>
	<header id="topo">
		<img src="img/logo.png" alt="Troco Simples" class="efeito">
	</header>

	<div class="conteudo efeito" id="intro">
		<strong>
			Conheça o jeito mais fácil e moderno 
			<br>de dar e receber troco.
		</strong>
		<p>
			<b>Troco Simples</b> é a melhor forma de acabar 
			<br>com o problema da <i>falta de moedas</i>.
		</p>
	</div>

	<div id="container">
		<div class="conteudo">
			<div id="varejo" class="box efeito">
				<h2>
					CHEGA DE FALTA DE 
					<br>MOEDAS NO VAREJO
				</h2>

				<p>
					A <b>Troco Simples</b> roda integrada ao seu frente de caixa (PDV) e ou TEF, para que você  entregue <b>TROCO DIGITAL</b> direto no CPF dos seus clientes.
				</p>

				<span>
					Quer saber as vantagens de usar a 
					<br><b>Troco Simples</b> para a sua empresa? 
				</span>

				<!--Link AQUI, substitua a #-->
				<a href="http://www.trocosimples.com.br/landing-empresa/" target="_blank">
					<button>Então clique aqui!</button>
				</a>
			</div>

			<div id="bala" class="box efeito">
				<h2>
					PARE AGORA MESMO DE 
					<br>RECEBER TROCO EM BALA
				</h2>

				<p>
					Com <b>Troco Simples</b> você recebe 
					<br>troco digital direto no seu CPF 
					<br>e usa como quiser.
				</p>

				<span>
					Quer saber as vantagens de usar a 
					<br><b>Troco Simples</b> pra você?
				</span>

				<!--Link AQUI, substitua a #-->
				<a href="http://www.trocosimples.com.br/landing-usuario/" target="_blank">
					<button>Então clique aqui!</button>
				</a>
			</div>
		<br>
		</div>
	</div>

	<footer id="rodape">
		<img src="img/logo.png" alt="Troco Simples" id="marca" class="efeito">
		<br>contato@trocosimples.com.br | (41) 3514 4930
		<ul>
			<li>
				<a href="https://www.facebook.com/trocosimples" target="_blank" class="soft-hover">
					<img src="img/fb.png" class="rede"> 
					<span>/trocosimples</span>
				</a>
			</li>
			<li>
				<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank" class="soft-hover">
					<img src="img/ig.png" class="rede">
					<span>@trocosimplesoficial</span>
				</a>
			</li>
		</ul>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

	<script src="js/main.js" type="text/javascript"></script>
</body>
</html>