<!doctype html>
<html lang="pt-br" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="index,follow">
	<meta name="author" content="Ueek Agência Digital">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0">

	<title>Troco Simples - Aplicativo troco digital</title>
	<meta name='description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>

	<meta property='og:title' content='Troco Simples - Aplicativo troco digital'>
	<meta property='og:type' content='website'>
	<meta property='og:site_name' content='Troco Simples'>
	<meta property='og:description' content='O cofrinho que faz seu troco render mais dinheiro, na palma da sua mão.'>

	<link rel="shortcut icon" href="img/icon.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">

	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/mqueries.css">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-T8S7JRG');</script>
	<!-- End Google Tag Manager -->
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8S7JRG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header id="topo">
		<img src="img/logo.png" alt="Troco Simples" class="efeito">
	</header>

	<section id="cofrinho" class="conteudo">
		<div id="imagem-mobile" class="efeito"></div>

		<div id="txt" class="efeito">
			<h2>
				O “cofrinho” que faz seu<br>
				troco render mais<br>
				dinheiro, na palma da<br>
				sua mão.
			</h2>

			<span>
				Faça o download do app.
			</span>

			<a href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8" target="_blank">
				<img src="img/app.png" class="mobile soft-hover">
			</a>

			<a href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor" target="_blank">
				<img src="img/google.png" class="mobile soft-hover">
			</a>
		</div>

		<div id="imagem" class="efeito"></div>
	<br>
	</section>

	<section id="moedas" class="conteudo">
		<div id="imagem" class="efeito"></div>

		<div id="txt" class="efeito">
			<h2>
				Pare de carregar ou
				<br>perder moedas por aí.
			</h2>

			<span>
				Receba troco digital no CPF e escolha 
				<br>o melhor jeito de usar.
			</span>

			<h3>
				Sem download.
				<br>Sem cadastro.
				<br>Sem custo.
			</h3>
		</div>
	<br>
	</section>


	<section id="beneficios">
		<div class="conteudo">
			<div class="coluna efeito">
				<div class="item" id="transferencia">
					Transferência automática
					<br>para conta bancária.
				</div>

				<div class="item" id="pague">
					Pague sua conta na rede 
					<br>de parceiros.
				</div>

				<div class="item" id="rachou">
					Se rachou a conta, envie 
					<br>seu saldo para um amigo.
				</div>
			</div>

			<div class="coluna efeito">
				<div class="item" id="acumule">
					Acumule troco no app e tenha
					<br>um rendimento de até 6% a.a.
				</div>

				<div class="item" id="recarregue">
					Recarregue seu celular
					<br>pré-pago.
				</div>

				<div class="item" id="doe">
					Doe a segunda casa depois da vírgula
					<br>para instituições de Caridade.
				</div>
			</div>
		<br>
		</div>
	</section>

	<section id="download" class="conteudo">
		<div id="botoes" class="efeito">
			<a href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8" target="_blank">
				<img src="img/app.png" class="mobile soft-hover">
			</a>

			<a href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor" target="_blank">
				<img src="img/google.png" class="mobile soft-hover">
			</a>
		</div>

		<b>Chega de troco em bala!</b>
		<br>
		Agora seu troco é de um jeito muito mais moderno:
		<br><br>
		www.trocosimples.com.br   
	</section>

	<footer id="rodape">
		<img src="img/logo.png" alt="Troco Simples" id="marca" class="efeito">
		<br>contato@trocosimples.com.br | (41) 3514 4930
		<ul>
			<li>
				<a href="https://www.facebook.com/trocosimples" target="_blank" class="soft-hover">
					<img src="img/fb.png" class="rede"> 
					<span>/trocosimples</span>
				</a>
			</li>
			<li>
				<a href="https://www.instagram.com/trocosimplesoficial/" target="_blank" class="soft-hover">
					<img src="img/ig.png" class="rede">
					<span>@trocosimplesoficial</span>
				</a>
			</li>
		</ul>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

	<script src="js/main.js" type="text/javascript"></script>
</body>
</html>