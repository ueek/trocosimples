<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-revendedores.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="seja-revendedor">
	<div class="conteudo">

		<span><a href="index.php?pg=revendedores"><img src="img/voltar.png" alt="TrocoSimples"></a> Revendedores</span>

		<h1>Seja um revendedor</h1>

		<p>
			A Troco Simples é uma solução de tecnologia financeira, mas isso não a torna complexa, pelo contrário, automatizamos as integrações com sistemas de mercado e de destino ao troco acumulado pelos consumidores.<br>
			Com isso, o papel do revendedor Troco Simples, não está relacionado ao seu conhecimento em tecnologia ou finanças, mas sim em relacionamento e organização, dividindo-se da seguinte forma:
		</p>
		
		<div class="display-desktop">
			<img id="um" src="img/linha-revendedores.png" alt="TrocoSimples">			
		</div>
 
		<div class="display-mobile">
			<img id="um" src="img/linha-revendedores-mobile.png" alt="TrocoSimples">			
		</div>

		<ul>
			<li>Apoiar parceiros de sistema e de distribuição a comercializarem a Troco Simples para seus clientes;</li>
			<li>Conquistar clientes âncoras e estratégicos a fim de popularizar a Troco Simples na sua região;</li>
			<li>Buscar parcerias com empresas de sistemas regionais;</li>
			<li>Realizar a ativação dos clientes.</li>
		</ul>

		<div class="display-desktop">
			<img id="dois" src="img/linha-revendedores.png" alt="TrocoSimples">			
		</div>

		<div class="display-mobile">
			<img id="dois" src="img/linha-revendedores-mobile.png" alt="TrocoSimples">			
		</div>

		<p>
			A Ativação de Clientes é o papel mais importante neste processo, pois é a partir dela que a Troco Simples se propagará na sua região, pelo boca a boca, como solução para a falta de moedas no mercado.
			<br><br>
			Por ativação, compreendemos o papel de treinar os profissionais das lojas, identificar a loja com os materiais da Troco Simples, alocar promotoras conforme necessidade, entre outras ações operacionais, sendo obviamente esse valor subsidiado com parte do valor pago pelo cliente (varejo) contratante.
			<br><br>
			Se você possui conhecimento da sua região, principalmente com acesso a varejistas, e está disposto a iniciar um projeto que promoverá a redução de custos e comodidade a todos os envolvidos, converse com a gente e torne-se você também um de nossos revendedores:
		</p>

	</div>
</section>

<section class="formularios">
	<div class="conteudo">
		<h1>PARA EMPRESAS</h1>

		<h2>Saia do prejuízo pela falta de moedas</h2>

		<p>
			Rodamos integrados ao seu frente de caixa (PDV) e ou TEF,  para que você entregue troco digital direto no CPF dos seus clientes.
		</p>

		<form id="cadastrar-revendedor" class="formulario">
			<div class="col-1">
				
				<label>Nome*</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>Último Nome*</label>
				<input class="soft-hover" type="text" name="sobrenome" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="fone" placeholder="">
			</div>
			<div class="col-2">				

				<label>Estado*</label>
				<select id="estado" name="estado">
					<option value="">UF</option>
					<option value="AC">AC</option>
					<option value="AL">AL</option>
					<option value="AP">AP</option>
					<option value="AM">AM</option>
					<option value="BA">BA</option>
					<option value="CE">CE</option>
					<option value="DF">DF</option>
					<option value="ES">ES</option>
					<option value="GO">GO</option>
					<option value="MA">MA</option>
					<option value="MT">MT</option>
					<option value="MS">MS</option>
					<option value="MG">MG</option>
					<option value="PA">PA</option>
					<option value="PB">PB</option>
					<option value="PR">PR</option>
					<option value="PE">PE</option>
					<option value="PI">PI</option>
					<option value="RJ">RJ</option>
					<option value="RN">RN</option>
					<option value="RS">RS</option>
					<option value="RO">RO</option>
					<option value="RR">RR</option>
					<option value="SC">SC</option>
					<option value="SP">SP</option>
					<option value="SE">SE</option>
					<option value="TO">TO</option>
				</select>

				<label>Mensagem*</label>
				<textarea class="soft-hover" type="text" name="mensagem" placeholder=""></textarea>

				<label>Currículo*</label>
				<input class="soft-hover" type="file" id="curriculo"  name="curriculo" placeholder="">

			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->