<?php
    include_once 'models/blog-post.php';
    include_once 'models/blog-post-foto.php';
    include_once 'config/database.php';

	// Instanciar objeto
    $blogPost = new BlogPost($db);

    //Verifica se a variável $_GET['categoria'] não está vazia
	if(isset($_GET['url'])){		
    	$myBlogPost = $blogPost->readByUrl($_GET['url']);
	}

	$urlImagem = "/gestor/uploads/blog-posts/"; 
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-blog.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section class="publicacao">
	<div class="conteudo">
		<span><a href="/blog"><img src="/img/voltar.png" alt="TrocoSimples"></a> Blog</span>

		<div class="post">
			<?php

				while ($row = $myBlogPost->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					$post_id = $id;

					$imagemPrincipal = $urlImagem.$imagem;

					$textoFormatado = nl2br($texto);

					echo "
						<h1>$titulo</h1>
						
						<a data-fancybox='group' href='$imagemPrincipal'>
							<img class='' src='$imagemPrincipal' alt='TrocoSimples'>
						</a>

						<p class=''>
							$textoFormatado
						</p>
					";
				}
			?>


		</div>
		<br>

		<div class="imagens-post regular4">

				<?php 

					$foto = new BlogPostFoto($db);

					$fotos = $foto->readAllByBlogPostId($post_id);

					while ($row = $fotos->fetch(PDO::FETCH_ASSOC)){
						extract($row);

						$imagemUrl = $urlImagem.$imagem;

						echo"
							<div>
								<a data-fancybox='group' href='$imagemUrl'>
									<div class='img-post' style='background: url($imagemUrl); background-position: center; background-repeat: no-repeat; background-size: cover;'></div>
								</a>
							</div>					
						";
					}

				?>		
			<br>
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->