<?php
	
    //Arquivos externos
    include_once '../models/revendedor-candidato.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myCandidato = new RevendedorCandidato($db);

	if (isset($_GET['id_candidato'])) {
	    // Buscar dados do candidato
	    $stmtCandidato = $myCandidato->readById($_GET['id_candidato']);


	    if ($stmtCandidato->rowCount() > 0) {
		    $row = $stmtCandidato->fetch(PDO::FETCH_ASSOC);
			extract($row);
			$myCandidato->setId($id);
			$myCandidato->setNome($nome);
			$myCandidato->setCidade($cidade);
			$myCandidato->setEmail($email);
			$myCandidato->setMensagem($mensagem);
			$myCandidato->setSobrenome($sobrenome);
			$myCandidato->setFone($fone);
			$myCandidato->setCurriculo($curriculo);
					    
	    }
	}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Revendedor</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-revendedor" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Revendedor -->
				<div class="form-group">
					<label>Revendedor*</label>
					<input class="form-control" type="text" placeholder="Revendedor" name="nome" value="<?php  echo $myCandidato->getNome()." ".$myCandidato->getSobrenome() ?>">
				</div>

				<!-- Cidade/UF -->
				<div class="form-group">
					<label>Cidade/UF*</label>
					<input class="form-control" type="text" placeholder="Cidade/UF" name="cidade-uf" value="<?php  echo $myCandidato->getCidade() ?>">
				</div>

				<!-- Fone -->
				<div class="form-group">
					<label>Fone*</label>
					<input class="form-control" type="text" placeholder="Fone" name="fone" value="<?php  echo $myCandidato->getFone() ?>">
				</div>

				<!-- E-mail -->
				<div class="form-group">
					<label>E-mail*</label>
					<input class="form-control" type="text" placeholder="E-mail" name="email" value="<?php  echo $myCandidato->getEmail() ?>">
				</div>				

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=revendedores'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Revendedores
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Revendedor" id="botao">
		</form>
	</div>
</div>