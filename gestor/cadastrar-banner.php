<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Banner</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-banner" enctype="multipart/form-data">
			
			<div id="fields">

				<div id="imagens-upload">
					<!-- Título -->
					<div class="form-group">
						<label>Título*</label>
						<input class="form-control" type="text" placeholder="Título" name="titulo">
					</div>

					<!-- Subtítulo -->
					<div class="form-group">
						<label>Subtítulo*</label>
						<input class="form-control" type="text" placeholder="Subtítulo" name="subtitulo">
					</div>

					<!-- Texto do Botão -->
					<div class="form-group">
						<label>Texto do Botão*</label>
						<input class="form-control" type="text" placeholder="Texto do Botão" name="texto">
					</div>

					<!-- Link (URL) -->
					<div class="form-group">
						<label>Link (URL)*</label>
						<input class="form-control" type="text" placeholder="Link (URL)" name="url">
					</div>

					<!-- Imagem -->
					<div class="form-group">
						<label>Imagem (Dimensões recomendadas: 1920x965)*</label>
						<input type="file" name="imagem1" id="imagem1">
					</div>

				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=banners'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Banners
				</button>
			</a>

            <input type="submit" class="btn btn-success direita" value="Cadastrar Banner" id="botao">
            <br><br>
		</form>
	</div>
</div>