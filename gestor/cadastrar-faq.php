<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">FAQ</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-faq" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo">
				</div>

				<!-- Texto -->
				<div class="form-group">
					<label>Texto</label>
					<textarea class="form-control" name="texto" placeholder="Texto" ></textarea>
					<div class="restantes">
						<span id="caracteres-restantes">Máximo 2000</span> caracteres
					</div>
				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=faqs'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para FAQs
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar FAQ" id="botao">
		</form>
	</div>
</div>