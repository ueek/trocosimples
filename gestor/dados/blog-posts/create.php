<?php
    //Arquivos externos
    include_once '../../../models/blog-post.php';
    include_once '../../../models/blog-categoria.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlogPost = new BlogPost($db);

	$myBlogPost->setTitulo($_POST['titulo']);
	$myBlogPost->setTexto($_POST['texto']);
	$myBlogPost->setUrl(urlAmigavel($myBlogPost->getTitulo()));

	// Categorias do Post
    $categoria = new BlogCategoria($db);
    $categorias = $categoria->read();

    $categoriasSelecionadas = "";
    while ($row = $categorias->fetch(PDO::FETCH_ASSOC)){
		extract($row);

		if (isset($_POST['categoria-'.$id])) {
			$categoriasSelecionadas = $categoriasSelecionadas.$url.",";
		}
	}
	$myBlogPost->setCategoriaIds($categoriasSelecionadas);

	if (!$myBlogPost->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
