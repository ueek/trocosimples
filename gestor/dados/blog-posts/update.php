<?php
    //Arquivos externos
    include_once '../../../models/blog-post.php';
    include_once '../../../models/blog-categoria.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlogPost = new BlogPost($db);

	$myBlogPost->setId($_POST['blog-post-id']);
	$myBlogPost->setTitulo($_POST['titulo']);
	$myBlogPost->setTexto($_POST['texto']);
	$myBlogPost->setUrl(urlAmigavel($myBlogPost->getTitulo()));

	// Categorias do Post
    $categoria = new BlogCategoria($db);
    $categorias = $categoria->read();

    $categoriasSelecionadas = "";
    while ($row = $categorias->fetch(PDO::FETCH_ASSOC)){
		extract($row);

		if (isset($_POST['categoria-'.$id])) {
			$categoriasSelecionadas = $categoriasSelecionadas.$url.",";
		}
	}
	$myBlogPost->setCategoriaIds($categoriasSelecionadas);

	if (!$myBlogPost->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}





?>
