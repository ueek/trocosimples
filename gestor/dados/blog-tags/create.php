<?php
    //Arquivos externos
    include_once '../../../models/blog-tag.php';
    include_once '../../../config/database.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlogTag = new BlogTag($db);

	$myBlogTag->setNome($_POST['blog-tag']);

	if (!$myBlogTag->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
