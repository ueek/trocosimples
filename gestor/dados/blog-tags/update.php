<?php
    //Arquivos externos
    include_once '../../../models/blog-tag.php';
    include_once '../../../config/database.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlogTag = new BlogTag($db);


	$myBlogTag->setId($_POST['blog-tag-id']);
	$myBlogTag->setNome($_POST['blog-tag']);

	if (!$myBlogTag->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
