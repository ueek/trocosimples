<?php
    //Arquivos externos
    include_once '../../../models/blog-categoria.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlogCategoria = new BlogCategoria($db);

	$myBlogCategoria->setNome($_POST['blog-categoria']);
	$myBlogCategoria->setUrl(urlAmigavel($myBlogCategoria->getNome()));

	if (!$myBlogCategoria->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
