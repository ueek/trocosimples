<?php
    //Arquivos externos
    include_once '../../../models/faq.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myFaq = new Faq($db);


	$myFaq->setId($_POST['faq-id']);
	$myFaq->setTitulo($_POST['titulo']);
	$myFaq->setTexto($_POST['texto']);

	if (!$myFaq->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
