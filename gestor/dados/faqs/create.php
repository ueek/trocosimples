<?php
    //Arquivos externos
    include_once '../../../models/faq.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//
	// $retorno = array("status" => 0, "msg" => $_POST['texto']);
	// echo json_encode($retorno);
	// exit;
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myFaq = new Faq($db);

	$myFaq->setTitulo($_POST['titulo']);
	$myFaq->setTexto($_POST['texto']);

	if (!$myFaq->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
