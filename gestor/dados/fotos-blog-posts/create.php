<?php
    //Arquivos externos
    include_once '../../../models/blog-post-foto.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Preencher propriedades
	$myNoticiaId = $_POST['blog-post-id'];

    $id_Imagem1 = $_POST['id_Imagem1'];
    $id_Imagem2 = $_POST['id_Imagem2'];
    $id_Imagem3 = $_POST['id_Imagem3'];
    $id_Imagem4 = $_POST['id_Imagem4'];
    $id_Imagem5 = $_POST['id_Imagem5'];

    $imagem = new BlogPostFoto($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------
	$imagem1 = uploadImagem ("imagem1", "../../uploads/blog-posts/");
	if($imagem1 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($imagem1 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($imagem1 == 3 || $imagem1 == 4){
		$imagem1 = "";
	}


	// Upload imagem 2
	// ----------------------------------------------------------------------------------------------------------------------
	$imagem2 = uploadImagem ("imagem2", "../../uploads/blog-posts/");

	if($imagem2 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 2 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($imagem2 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 2 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($imagem2 == 3 || $imagem2 == 4){
		$imagem2 = "";
	}

	// Upload imagem 3
	// ----------------------------------------------------------------------------------------------------------------------
	$imagem3 = uploadImagem ("imagem3", "../../uploads/blog-posts/");
	if($imagem3 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 3 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($imagem3 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 3 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($imagem3 == 3 || $imagem3 == 4){
		$imagem3 = "";
	}

		// Upload imagem 4
	// ----------------------------------------------------------------------------------------------------------------------
	$imagem4 = uploadImagem ("imagem4", "../../uploads/blog-posts/");
	if($imagem4 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 4 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($imagem4 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 4 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($imagem4 == 3 || $imagem4 == 4){
		$imagem4 = "";
	}

	// Upload imagem 5
	// ----------------------------------------------------------------------------------------------------------------------
	$imagem5 = uploadImagem ("imagem5", "../../uploads/blog-posts/");
	if($imagem5 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 5 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($imagem5 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 5 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($imagem5 == 3 || $imagem5 == 4){
		$imagem5 = "";
	}


	// Salvar imagem 1
	if ( $imagem1 != "") {
	    $imagem->setImagem($imagem1);
	    $imagem->setBlogPostId($myNoticiaId);

		// Alterar ou incluir imagem
		if ($id_Imagem1 != 0) {
			$imagem->setId($id_Imagem1);
			if (!$imagem->update()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
		else {
			$imagem->Tipo = 1;
			if (!$imagem->create()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
	}

    //Salvar imagem 2
	if ( $imagem2 != "") {
		// Instanciar objeto
	    $imagem = new BlogPostFoto($db);
	    $imagem->setImagem($imagem2);
	    $imagem->setBlogPostId($myNoticiaId);

		// Alterar ou incluir imagem
		if ($id_Imagem2 != 0) {
			$imagem->setId($id_Imagem2);
			if (!$imagem->update()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
		else {
			if (!$imagem->create()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}

	}

    //Salvar imagem 3
	if ( $imagem3 != "") {
		// Instanciar objeto
	    $imagem = new BlogPostFoto($db);
	    $imagem->setImagem($imagem3);
	    $imagem->setBlogPostId($myNoticiaId);

		// Alterar ou incluir imagem
		if ($id_Imagem3 != 0) {
			$imagem->setId($id_Imagem3);
			if (!$imagem->update()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
		else {
			if (!$imagem->create()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
	}

    //Salvar imagem 4
	if ( $imagem4 != "") {
		// Instanciar objeto
	    $imagem = new BlogPostFoto($db);
	    $imagem->setImagem($imagem4);
	    $imagem->setBlogPostId($myNoticiaId);

		// Alterar ou incluir imagem
		if ($id_Imagem4 != 0) {
			$imagem->setId($id_Imagem4);
			if (!$imagem->update()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
		else {
			if (!$imagem->create()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
	}

    //Salvar imagem 5
	if ( $imagem5 != "") {
		// Instanciar objeto
	    $imagem = new BlogPostFoto($db);
	    $imagem->setImagem($imagem5);
	    $imagem->setBlogPostId($myNoticiaId);

		// Alterar ou incluir imagem
		if ($id_Imagem5 != 0) {
			$imagem->setId($id_Imagem5);
			if (!$imagem->update()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
		else {
			if (!$imagem->create()){
				$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao incluir a(s) imagen(s), tente novamente.");
				echo json_encode($retorno);
				exit;
			}
		}
	}


	$retorno = array("status" => 1, "msg" => "Imagens alteradas com sucesso!");
	echo json_encode($retorno);
	exit;

?>
