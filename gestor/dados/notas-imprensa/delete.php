<?php
    //Arquivos externos
    include_once '../../../models/nota-imprensa.php';
    include_once '../../../config/database.php';

	//==============================================================//
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myNotaImprensa = new NotaImprensa($db);

	// Excluir imagem
	if ($myNotaImprensa->delete($_POST['id'])) {
		$retorno = array("status" => 1, "msg" => "Item excluído com sucesso!");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao excluir este item, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>
