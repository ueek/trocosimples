<?php
    //Arquivos externos
    include_once '../../../models/revendedor.php';
    include_once '../../../config/database.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myRevendedor = new Revendedor($db);

	$myRevendedor->setNome($_POST['nome']);
	$myRevendedor->setCidadeUf($_POST['cidade-uf']);
	$myRevendedor->setFone($_POST['fone']);
	$myRevendedor->setEmail($_POST['email']);
	

	if (!$myRevendedor->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
