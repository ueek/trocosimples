<?php
    //Arquivos externos
    include_once '../../../models/sistema.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $mySistema = new Sistema($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------

	if (!isset($_POST['imagem1-old'])) {



		$imagem1 = uploadImagem ("imagem1", "../../uploads/sistemas/");
		if($imagem1 == 1) {
			$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
			echo json_encode($retorno);
			exit;
		}else if($imagem1 == 2){
			$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
			echo json_encode($retorno);
			exit;
		}else if($imagem1 == 3 || $imagem1 == 4){
			$imagem1 = "";
		}

	}else{
		$imagem1 = $_POST['imagem1-old'];
	}



	$mySistema->setId($_POST['sistema-id']);
	$mySistema->setNome($_POST['sistema']);
	$mySistema->setTipo($_POST['tipo']);
	$mySistema->setImagem($imagem1);

	if (!$mySistema->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
