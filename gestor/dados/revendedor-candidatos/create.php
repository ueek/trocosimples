<?php
    //Arquivos externos
    include_once '../../../models/revendedor-candidato.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-pdf.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myCandidato = new RevendedorCandidato($db);

	// Upload do arquivo
	$arquivo = uploadArquivo("curriculo", "../../uploads/curriculos/", 1000000);
	if($arquivo == 1) {
		$retorno = array("status" => 0, "msg" => "Arquivo muito grande");
		echo json_encode($retorno);
		exit;
	}else if($arquivo == 2){
		$retorno = array("status" => 0, "msg" => "Arquivo possui uma extensão diferente de pdf!");
		echo json_encode($retorno);
		exit;
	}else if($arquivo == 3 || $arquivo == 4){
		$arquivo = "";
	}

	// Se o upload for efetuado corretamente, seta no objeto para salvar no BD
	$myCandidato->setNome($_POST['nome']);
	$myCandidato->setCidade($_POST['cidade'].", ".$_POST['estado']);
	$myCandidato->setEmail($_POST['email']);
	$myCandidato->setMensagem($_POST['mensagem']);
	$myCandidato->setSobrenome($_POST['sobrenome']);
	$myCandidato->setFone($_POST['fone']);
	$myCandidato->setCurriculo($arquivo);

	if (!$myCandidato->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
