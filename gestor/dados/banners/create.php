<?php
    //Arquivos externos
    include_once '../../../models/banner.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/limpa-url.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBanner = new Banner($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------
	$imagem1 = uploadImagem ("imagem1", "../../uploads/banners/");
	if($imagem1 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($imagem1 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($imagem1 == 3 || $imagem1 == 4){
		$imagem1 = "";
	}

	$myBanner->setTitulo($_POST['titulo']);
	$myBanner->setSubtitulo($_POST['subtitulo']);
	$myBanner->setTexto($_POST['texto']);
	$myBanner->setUrl($_POST['url']);
	$myBanner->setImagem($imagem1);


	if (!$myBanner->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
