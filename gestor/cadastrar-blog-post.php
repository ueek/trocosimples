<?php
    //Arquivos externos
    include_once '../models/blog-categoria.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $categoria = new BlogCategoria($db);

    $categorias = $categoria->read("nome");
   
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Novo Post</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-blog-post" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo">
				</div>

				<!-- Categorias -->
				<div class="form-group">
	                <label>Selecione as Categorias</label>
	                <br>

	                <?php 
		        		if ($categorias->rowCount() > 0) {
					    	while ($row = $categorias->fetch(PDO::FETCH_ASSOC)){
					    		
					    		extract($row);

					    		echo "
					                <label class='checkbox-inline'>
					                    <input type='checkbox' name='categoria-{$id}' id='categoria-{$id}'>{$nome}
					                </label>
					    		";
					    	}
					    }else{
				    		echo "
				                    Nenhuma categoria cadastrada.
				    		";
					    }
		        	?>
	            </div>

				<!-- Texto -->
				<div class="form-group">
					<label>Texto</label>
					<textarea class="form-control" id="texto" name="texto" placeholder="Texto" maxlength="10000"></textarea>

					<div class="restantes">
						<span id="caracteres-restantes">Máximo 10000</span> caracteres
					</div>
				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=blog-posts'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Posts
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Post" id="botao">
            <br><br>
		</form>
	</div>
</div>