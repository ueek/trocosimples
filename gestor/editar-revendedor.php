<?php
	
    //Arquivos externos
    include_once '../models/revendedor.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $revendedor = new Revendedor($db);

	if (isset($_GET['id'])) {
	    // Buscar dados do revendedor
	    $stmtCandidato = $revendedor->readById($_GET['id']);


	    if ($stmtCandidato->rowCount() > 0) {
		    $row = $stmtCandidato->fetch(PDO::FETCH_ASSOC);
			extract($row);
			$revendedor->setId($id);
			$revendedor->setNome($nome);
			$revendedor->setCidadeUf($cidade_uf);
			$revendedor->setFone($fone);
			$revendedor->setEmail($email);
					    
	    }
	}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Revendedor</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-revendedor" enctype="multipart/form-data">
			
			<div id="fields">

				<input type="hidden" value="<?php  echo $revendedor->getId() ?>" name="revendedor-id">

				<!-- Revendedor -->
				<div class="form-group">
					<label>Revendedor*</label>
					<input class="form-control" type="text" placeholder="Revendedor" name="nome" value="<?php  echo $revendedor->getNome() ?>">
				</div>

				<!-- Cidade/UF -->
				<div class="form-group">
					<label>Cidade/UF*</label>
					<input class="form-control" type="text" placeholder="Cidade/UF" name="cidade-uf" value="<?php  echo $revendedor->getCidadeUf() ?>">
				</div>

				<!-- Fone -->
				<div class="form-group">
					<label>Fone*</label>
					<input class="form-control" type="text" placeholder="Fone" name="fone" value="<?php  echo $revendedor->getFone() ?>">
				</div>

				<!-- E-mail -->
				<div class="form-group">
					<label>E-mail*</label>
					<input class="form-control" type="text" placeholder="E-mail" name="email" value="<?php  echo $revendedor->getEmail() ?>">
				</div>				

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=revendedores'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Revendedores
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Alterar Revendedor" id="botao">
		</form>
	</div>
</div>