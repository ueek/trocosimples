<?php
    //Arquivos externos
    include_once '../models/revendedor-candidato.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $candidato = new RevendedorCandidato($db);

    $candidatos = $candidato->read();
   
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Revendedores - Candidatos</h1>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<b>Todos os dados cadastrados pelo site são exibidos aqui.</b> Para visualizar as informações, clique no botão azul. Para criar um Revendedor a partir de um Candidato, clique no botão verde. Para remover, clique no botão vermelho.

		<br><br>

        <div class="panel panel-default">
            <div class="panel-heading">
            	Revendedores - Candidatos Cadastrados
            </div>

            <div class="panel-body">
			    <table width="100%" class="table table-striped table-bordered table-hover tabela-lobee">
			        <thead>
			            <tr>
			                <th>ID</th>
			                <th>Nome</th>
			                <th>Cidade/UF</th>
			                <th>Email</th>
			                <th>Fone</th>
			                <th>Data de Cadastro</th>
			                <th>Ações</th>
			            </tr>
			        </thead>
			        <tbody>
			        	<?php 

		        			
			        		if ($candidatos->rowCount() > 0) {
						    	while ($row = $candidatos->fetch(PDO::FETCH_ASSOC)){
						    		
						    		extract($row);

						    		echo "
						    			<tr>
							                <td>$id</td>
							                <td>$nome</td>
							                <td>$cidade</td>
							                <td>$email</td>
							                <td>$fone</td>
							                <td>$cadastrado_em</td>
							                <td>
							                  <a href='index.php?pg=visualizar-candidato-revendedor&id=$id'>
							                      <button type='button' class='btn btn-info btn-circle'>
							                          <i class='fa fa-eye'></i>
							                      </button>
							                  </a>
  							                  <a href='index.php?pg=cadastrar-revendedor&id_candidato=$id'>
							                      <button type='button' class='btn btn-success btn-circle'>
							                          <i class='fa fa-external-link'></i>
							                      </button>
							                  </a>
							                  <button type='button' class='btn btn-danger btn-circle remover-candidato-revendedor'>
							                      <i class='fa fa-remove'></i>
							                      <input type='hidden' value='$id' name='id'>
							                  </button>
							                </td>
							            </tr>
						    		";
						    	}
						    }
			        	?>
			        </tbody>
			    </table>
	    	</div>
        </div>
    </div>
</div>