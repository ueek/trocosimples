<?php
	
	    //Arquivos externos
    include_once '../models/blog-post.php';
    include_once '../models/blog-categoria.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $blogPost = new BlogPost($db);

    // Buscar dados do blogPost
    $stmtBlogPost = $blogPost->readById($_GET['id']);

    if ($stmtBlogPost->rowCount() > 0) {
	    $row = $stmtBlogPost->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$blogPost->setId($id);
		$blogPost->setTitulo($titulo);
		$blogPost->setTexto($texto);
		$blogPost->setCategoriaIds($categoria_ids);
    }

    $categoria = new BlogCategoria($db);
    $categorias = $categoria->read("nome");

?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Post</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-blog-post" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $blogPost->getId() ?>" name="blog-post-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo" value="<?php  echo $blogPost->getTitulo() ?>">
				</div>

				<!-- Categorias -->
				<div class="form-group">
	                <label>Selecione as Categorias</label>
	                <br>

	                <?php 
		        		if ($categorias->rowCount() > 0) {

				    		$arr_categorias = explode(",", $blogPost->getCategoriaIds());

					    	while ($row = $categorias->fetch(PDO::FETCH_ASSOC)){
					    		
					    		extract($row);

					    		if (in_array($url, $arr_categorias)) {
					    			$checked = "checked='checked'";
					    		}else{
					    			$checked = "";
					    		}

					    		echo "
					                <label class='checkbox-inline'>
					                    <input type='checkbox' name='categoria-{$id}' id='categoria-{$id}' {$checked}>{$nome}
					                </label>
					    		";
					    	}
					    }else{
				    		echo "
				                    Nenhuma categoria cadastrada.
				    		";
					    }
		        	?>
	            </div>

	            <!-- Texto -->
				<div class="form-group">
					<label>Texto</label>
					<textarea class="form-control" id="texto" name="texto" placeholder="Texto" maxlength="10000"><?php  echo $blogPost->getTexto() ?></textarea>
					<div class="restantes">
						<span id="caracteres-restantes">Máximo 10000</span> caracteres
					</div>
				</div>

				


				<a href='index.php?pg=blog-posts'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Posts
					</button>
				</a>
				
	            <input type="submit" class="btn btn-success direita" value="Alterar Post" id="botao">
	            <br><br>
			</div>


		</form>
	</div>
</div>