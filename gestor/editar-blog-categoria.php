<?php
	
	    //Arquivos externos
    include_once '../models/blog-categoria.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $categoria = new BlogCategoria($db);

    // Buscar dados do categoria
    $stmtCategoria = $categoria->readById($_GET['id']);


    $urlImagens = "uploads/categorias/"; 

    if ($stmtCategoria->rowCount() > 0) {
	    $row = $stmtCategoria->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$categoria->setId($id);
		$categoria->setNome($nome);
        $imagem1 = $urlImagens.$imagem;
				    
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Categoria</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-blog-categoria" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $categoria->getId() ?>" name="blog-categoria-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Categoria -->
				<div class="form-group">
					<label>Categoria*</label>
					<input class="form-control" type="text" placeholder="Categoria" name="blog-categoria" value="<?php  echo $categoria->getNome() ?>">
				</div>

				<a href='index.php?pg=blog-categorias'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Categorias
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Categoria" id="botao">
	            
			</div>


		</form>
	</div>
</div>