<?php
	
	    //Arquivos externos
    include_once '../models/sistema.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $sistema = new Sistema($db);

    // Buscar dados do sistema
    $stmtSistema = $sistema->readById($_GET['id']);


    $urlImagens = "uploads/sistemas/"; 

    if ($stmtSistema->rowCount() > 0) {
	    $row = $stmtSistema->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$sistema->setId($id);
		$sistema->setNome($nome);
		$sistema->setTipo($tipo);
		$sistema->setImagem($imagem);
        $imagem1 = $urlImagens.$imagem;
				    
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Sistema</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-sistema" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $sistema->getId() ?>" name="sistema-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Sistema -->
				<div class="form-group">
					<label>Sistema*</label>
					<input class="form-control" type="text" placeholder="Sistema" name="sistema" value="<?php  echo $sistema->getNome() ?>">
				</div>

<!-- 
				$selected = "selected='selected'";
		                        }else{
		                            $selected = "";
		                        }

		                        echo"
		                        <option value='{$dadosCategorias['Id']}' {$selected}>{$dadosCategorias['Nome']}</option>
		                        "; -->

				<!-- Tipo -->
				<div class="form-group">
	                <label>Tipo*</label>
	                <select class="form-control" name="tipo">

	                	<?php 
	                		if ($sistema->getTipo() == 1) {
	                			$selected1 = "selected='selected'";
	                		}else{
	                			$selected2 = "selected='selected'";
	                		}

	                		echo "
		                    	<option value='1 '{$selected1}>TEF</option>
		                    	<option value='2 '{$selected2}>Frente de Caixa (PDV)</option>	
	                		";

	                	?>

	                </select>
	            </div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem Principal</label>
						<input type="file" name="imagem1" id="imagem1">

						<input type="hidden" value="<?php  echo $sistema->getImagem() ?>" name="imagem1-old">

						<?php 

							if ($imagem1 != "") {
								echo "

									<div class='thumb' style='background: url($imagem1) no-repeat center; background-size: cover;'>
					                </div>
					                <br>
		                		";
							}
						?>

					</div>

				</div>
				<a href='index.php?pg=sistemas'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Sistemas
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Sistema" id="botao">
	            
			</div>


		</form>
	</div>
</div>