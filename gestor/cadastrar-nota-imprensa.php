<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Notas da Imprensa</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-nota-imprensa" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo">
				</div>

				<!-- Resumo -->
				<div class="form-group">
					<label>Resumo</label>
					<textarea class="form-control" name="resumo" placeholder="Resumo" maxlength="500"></textarea>
					<div class="restantes">
						<span id="caracteres-restantes">Máximo 500</span> caracteres
					</div>
				</div>

				<!-- Link -->
				<div class="form-group">
					<label>Link*</label>
					<input class="form-control" type="text" placeholder="Link" name="url">
				</div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem <i>(Dimensões recomendadas: 965x400)</i>*</label>
						<input type="file" name="imagem1" id="imagem1">
					</div>

				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=notas-imprensa'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Notas da Imprensa
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Nota da Imprensa" id="botao">
		</form>
	</div>
</div>