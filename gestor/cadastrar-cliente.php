<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cliente</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-cliente" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Cliente -->
				<div class="form-group">
					<label>Cliente*</label>
					<input class="form-control" type="text" placeholder="Cliente" name="cliente">
				</div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem*</label>
						<input type="file" name="imagem1" id="imagem1">
					</div>

				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=clientes'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Clientes
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Cliente" id="botao">
		</form>
	</div>
</div>