<?php
    //Arquivos externos
    include_once '../models/blog-tag.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $tag = new BlogTag($db);

    $tags = $tag->read();
   
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tags</h1>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<b>Todos os dados cadastrados pelo gestor são exibidos aqui.</b> Para editar as informações, clique no botão amarelo. Para remover, clique no botão vermelho.
		<br><br><br>

		<a href='index.php?pg=cadastrar-blog-tag'>
			<button type='button' class='btn btn-success'>
			  <i class='fa fa-plus'></i>
			  Cadastrar Tag
			</button>
		</a>

		<br><br>

        <div class="panel panel-default">
            <div class="panel-heading">
            	Tags Cadastradas
            </div>

            <div class="panel-body">
			    <table width="100%" class="table table-striped table-bordered table-hover tabela-lobee">
			        <thead>
			            <tr>
			                <th>ID</th>
			                <th>Tag</th>
			                <th>Data de Cadastro</th>
			                <th>Ações</th>
			            </tr>
			        </thead>
			        <tbody>
			        	<?php 

		        			
			        		if ($tags->rowCount() > 0) {
						    	while ($row = $tags->fetch(PDO::FETCH_ASSOC)){
						    		
						    		extract($row);

						    		echo "
						    			<tr>
							                <td>$id</td>
							                <td>$nome</td>
							                <td>$cadastrado_em</td>
							                <td>
							                  <a href='index.php?pg=editar-blog-tag&id=$id'>
							                      <button type='button' class='btn btn-warning btn-circle'>
							                          <i class='fa fa-pencil'></i>
							                      </button>
							                  </a>
							                  <button type='button' class='btn btn-danger btn-circle remover-blog-tag'>
							                      <i class='fa fa-remove'></i>
							                      <input type='hidden' value='$id' name='id'>
							                  </button>
							                </td>
							            </tr>
						    		";
						    	}
						    }
			        	?>
			        </tbody>
			    </table>
	    	</div>
        </div>
    </div>
</div>