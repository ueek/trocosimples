<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Categoria</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-blog-categoria" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Categoria -->
				<div class="form-group">
					<label>Categoria*</label>
					<input class="form-control" type="text" placeholder="Categoria" name="blog-categoria">
				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=blog-categorias'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Categorias
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Categoria" id="botao">
		</form>
	</div>
</div>