<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Sistema</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-sistema" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Sistema -->
				<div class="form-group">
					<label>Sistema*</label>
					<input class="form-control" type="text" placeholder="Sistema" name="sistema">
				</div>

				<!-- Tipo -->
				<div class="form-group">
	                <label>Tipo*</label>
	                <select class="form-control" name="tipo">
                    	<option value='1'>TEF</option>
                    	<option value='2'>Frente de Caixa (PDV)</option>
	                </select>
	            </div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem*</label>
						<input type="file" name="imagem1" id="imagem1">
					</div>

				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=sistemas'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Sistemas
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Sistema" id="botao">
		</form>
	</div>
</div>