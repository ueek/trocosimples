<?php
	
	    //Arquivos externos
    include_once '../models/blog-post.php';
    include_once '../models/blog-post-foto.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $post = new BlogPost($db);

    // Buscar dados do post
    $stmtBlogPost = $post->readById($_GET['id']);


    $urlImagens = "uploads/blog-posts/"; 

    $id_Imagem1 = 0;
    $id_Imagem2 = 0;
    $id_Imagem3 = 0;
    $id_Imagem4 = 0;
    $id_Imagem5 = 0;
    $id_Imagem6 = 0;
    $id_Imagem7 = 0;

    if ($stmtBlogPost->rowCount() > 0) {
	    $row = $stmtBlogPost->fetch(PDO::FETCH_ASSOC);
		extract($row);

		$post->setId($id);
		$post->setTitulo($titulo);

	    $imagem = new BlogPostFoto($db);
		$stmtImagens = $imagem->readAllByBlogPostId($post->getId());

		if ($stmtImagens->rowCount() > 0) {

			$count = 1;
	    	while ($row = $stmtImagens->fetch(PDO::FETCH_ASSOC)){
	    		
	    		extract($row);

    			switch ($count) {
				    case 1:
				        $imagem1 = $urlImagens.$imagem;
    					$id_Imagem1 = $id;
				        break;
				    case 2:
				        $imagem2 = $urlImagens.$imagem;
    					$id_Imagem2 = $id;
				        break;
			        case 3:
				        $imagem3 = $urlImagens.$imagem;
    					$id_Imagem3 = $id;
				        break;
			        case 4:
				        $imagem4 = $urlImagens.$imagem;
    					$id_Imagem4 = $id;
				        break;
			        case 4:
				        $imagem4 = $urlImagens.$imagem;
    					$id_Imagem4 = $id;
				        break;
			        case 5:
				        $imagem5 = $urlImagens.$imagem;
    					$id_Imagem5 = $id;
				        break;
				}

	    		$count++;
	    	}
		}	
    }



?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Imagens BlogPost</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="imagens-blog-post" enctype="multipart/form-data">
			<input type="hidden" value="<?php  echo $post->getId() ?>" name="blog-post-id">
			<div class="form-group">
				<label>POST: <?php  echo $post->getTitulo() ?></label>
			</div>
			<br>

			<div id="imagens-upload">
				<div class="form-group">
					<label>Imagem 1 (Principal) <i>(Dimensões recomendadas: 965x400)</i></label>
					<input type="file" name="imagem1">
					<input type="hidden" name="id_Imagem1" value="<?php echo $id_Imagem1 ?>">

					<?php 

						if ($imagem1 != "") {
							echo "<div class='thumb' style='background: url($imagem1) no-repeat center; background-size: cover;'>
	                		</div> 

							<button type='button' class='btn btn-danger btn-circle remover-foto-blog-post'>
								<i class='fa fa-times'></i>
								<input type='hidden' value='$id_Imagem1' name='id_foto'>
							</button> Remover
			                <br>
	                		";
						}
					?>

				</div>

				<hr>
				<div class="form-group">
					<label>Imagem 2</label>
					<input type="file" name="imagem2">
					<input type="hidden" name="id_Imagem2" value="$id_Imagem2 ?>">

					<?php 

						if ($imagem2 != "") {
							echo "

								<div class='thumb' style='background: url($imagem2) no-repeat center; background-size: cover;'>
				                </div>

								<button type='button' class='btn btn-danger btn-circle remover-foto-blog-post'>
									<i class='fa fa-times'></i>
									<input type='hidden' value='$id_Imagem2' name='id_foto'>
								</button> Remover
				                <br>
	                		";
						}
					?>


				</div>

				<hr>
				<div class="form-group">
					<label>Imagem 3</label>
					<input type="file" name="imagem3">
					<input type="hidden" name="id_Imagem3" value="<?php echo $id_Imagem3 ?>">

					<?php 

						if ($imagem3 != "") {
							# code...
							echo "

								<div class='thumb' style='background: url($imagem3) no-repeat center; background-size: cover;'>
				                </div>

				                <button type='button' class='btn btn-danger btn-circle remover-foto-blog-post'>
									<i class='fa fa-times'></i>
									<input type='hidden' value='$id_Imagem3' name='id_foto'>
								</button> Remover
				                <br>
	                		";
						}
					?>


				</div>

				<hr>
				<div class="form-group">
					<label>Imagem 4</label>
					<input type="file" name="imagem4">
					<input type="hidden" name="id_Imagem4" value="<?php echo $id_Imagem4 ?>">


					<?php 

						if ($imagem4 != "") {
							# code...
							echo "

								<div class='thumb' style='background: url($imagem4) no-repeat center; background-size: cover;'>
				                </div>
								
								<button type='button' class='btn btn-danger btn-circle remover-foto-blog-post'>
									<i class='fa fa-times'></i>
									<input type='hidden' value='$id_Imagem4' name='id_foto'>
								</button> Remover
				                <br>


	                		";
						}
					?>
				</div>

				<hr>
				<div class="form-group">
					<label>Imagem 5</label>
					<input type="file" name="imagem5">
					<input type="hidden" name="id_Imagem5" value="<?php echo $id_Imagem5 ?>">


					<?php 

						if ($imagem5 != "") {
							# code...
							echo "

								<div class='thumb' style='background: url($imagem5) no-repeat center; background-size: cover;'>
				                </div>
								
								<button type='button' class='btn btn-danger btn-circle remover-foto-blog-post'>
									<i class='fa fa-times'></i>
									<input type='hidden' value='$id_Imagem5' name='id_foto'>
								</button> Remover
				                <br>


	                		";
						}
					?>
				</div>

			</div>

			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=blog-posts'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Posts
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Alterar Fotos" id="botao">
		</form>
	</div>
</div>