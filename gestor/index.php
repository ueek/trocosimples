<?php
    //Segurança
    include("../funcoes/ueek-protect.php");

    if(!isset($_GET["pg"]) && count($_GET) > 0){
        header("HTTP/1.0 404 Not Found");
        exit;
    }else if(isset($_GET["pg"])){
        if(pncHacker($_GET["pg"]) != ""){
            echo pncHacker($_GET["pg"]);
        }
    }

    //Nega Erro
    error_reporting(true);

    //Conexão
    include "../config/config_db.php";
    $conn = mysqli_connect($host, $login_bd, $senha_bd, $db);
           mysqli_set_charset($conn, "utf8");

    //Controle de Usuário
    include "../funcoes/verifica_usuario.php";

    //Funções Úteis
    include "../funcoes/converte-data.php";
    include "../funcoes/limita-caracteres.php";
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta name="author" content="Ueek Ag">
    <link rel="shortcut icon" href="../img/logo-min.png">

    <title>Gestor - Troco Simples</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/css/admin.css?version=14" rel="stylesheet">
    <link href="vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="lib/datepicker/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet">  

    <!-- include summernote css/js -->
    <link href="lib/summernote/summernote.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span id="marca-adm">Ueek.ADM - Troco Simples</span>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php?pg=home"><i class="fa fa-dashboard fa-fw"></i> Painel de Controle</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=banners"><i class="fa fa-image fa-fw"></i> Home Banners</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=blog-posts"><i class="fa fa-edit fa-fw"></i> Blog Posts</a>
                        </li>

                        <li>
                            <a href="index.php?pg=blog-categorias"><i class="fa fa-key fa-fw"></i> Blog Categorias</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=clientes"><i class="fa fa-users fa-fw"></i> Clientes</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=faqs"><i class="fa fa-question-circle fa-fw"></i> Sobre nós</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=notas-imprensa"><i class="fa fa-camera fa-fw"></i> Imprensa</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=campanhas"><i class="fa fa-bell fa-fw"></i> Campanhas</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=revendedores-candidatos"><i class="fa fa-hand-o-up fa-fw"></i> Revendedores - Candidatos</a>
                        </li>

                        <li>
                            <a href="index.php?pg=revendedores"><i class="fa fa-shopping-cart fa-fw"></i> Revendedores</a>
                        </li>

                        <br><li class="divider"></li>

                        <li>
                            <a href="index.php?pg=sistemas"><i class="fa fa-tasks fa-fw"></i> Sistemas</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <?php
                if(!isset($_GET['pg']) or empty($_GET['pg'])){
                    include("home.php");
                }else{
                    if($_SERVER['REQUEST_METHOD'] == 'GET'){
                        $link = "{$_GET['pg']}.php";
                        if(file_exists($link)){
                            include($link);
                        }else{
                            include("erro.php");
                        }
                    }else{
                        header("HTTP/1.0 404 Not Found");
                    }
                }
            ?>
        </div>

    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>
    <script src="dist/js/admin.js"></script>
    <script src="js/maskmoney.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <script src="../lib/autocomplete/cidades.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- DatePicker -->
    <script src="lib/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="lib/datepicker/js/moment.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- Charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

    <!-- Sistema -->
    <script src="js/mascaras.js?version=1"></script>

    <!-- Summernote -->
    <script src="lib/summernote/summernote.js"></script>  

    <script src="js/banners.js?version=1"></script>
    <script src="js/blog-posts.js?version=1"></script>
    <script src="js/fotos-blog-posts.js?version=1"></script>
    <script src="js/blog-categorias.js?version=1"></script>
    <script src="js/clientes.js?version=1"></script>
    <script src="js/faqs.js?version=1"></script>
    <script src="js/notas-imprensa.js?version=1"></script>
    <script src="js/revendedores.js?version=1"></script>
    <script src="js/revendedores-candidatos.js?version=1"></script>
    <script src="js/sistemas.js?version=1"></script>
    <script src="js/campanhas.js?version=1"></script>


</body>
</html>
