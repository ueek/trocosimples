<?php
	
	    //Arquivos externos
    include_once '../models/faq.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $faq = new Faq($db);

    // Buscar dados do faq
    $stmtFaq = $faq->readById($_GET['id']);


    $urlImagens = "uploads/faqs/"; 

    if ($stmtFaq->rowCount() > 0) {
	    $row = $stmtFaq->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$faq->setId($id);
		$faq->setTitulo($titulo);
		$faq->setTexto($texto);
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Faq</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-faq" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $faq->getId() ?>" name="faq-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo" value="<?php  echo $faq->getTitulo() ?>">
				</div>

				<!-- Texto -->
				<div class="form-group">
					<label>Texto</label>
					<textarea class="form-control" name="texto" placeholder="Texto" ><?php  echo $faq->getTexto() ?></textarea>
					<div class="restantes">
						<span id="caracteres-restantes">Máximo 2000</span> caracteres
					</div>
				</div>

				<a href='index.php?pg=faqs'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Faqs
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Faq" id="botao">
	            
			</div>


		</form>
	</div>
</div>