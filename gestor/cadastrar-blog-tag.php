<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tag</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="cadastrar-blog-tag" enctype="multipart/form-data">
			
			<div id="fields">

				<!-- Tag -->
				<div class="form-group">
					<label>Tag*</label>
					<input class="form-control" type="text" placeholder="Tag" name="blog-tag">
				</div>

			</div>


			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=blog-tags'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar para Tags
				</button>
			</a>


            <input type="submit" class="btn btn-success direita" value="Cadastrar Tag" id="botao">
		</form>
	</div>
</div>