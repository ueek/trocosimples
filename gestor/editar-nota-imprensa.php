<?php
	
	    //Arquivos externos
    include_once '../models/nota-imprensa.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $notaImprensa = new NotaImprensa($db);

    // Buscar dados do notaImprensa
    $stmtNotaImprensa = $notaImprensa->readById($_GET['id']);


    $urlImagens = "uploads/notas-imprensa/"; 

    if ($stmtNotaImprensa->rowCount() > 0) {
	    $row = $stmtNotaImprensa->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$notaImprensa->setId($id);
		$notaImprensa->setTitulo($titulo);
		$notaImprensa->setResumo($resumo);
		$notaImprensa->setUrl($url);
		$notaImprensa->setImagem($imagem);
        $imagem1 = $urlImagens.$imagem;
    }

?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Nota da Imprensa</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-nota-imprensa" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $notaImprensa->getId() ?>" name="nota-imprensa-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo" value="<?php  echo $notaImprensa->getTitulo() ?>">
				</div>

				<!-- Resumo -->
				<div class="form-group">
					<label>Resumo</label>
					<textarea class="form-control" name="resumo" placeholder="Resumo" maxlength="500"><?php  echo $notaImprensa->getResumo() ?></textarea>
					<div class="restantes">
						<span id="caracteres-restantes">Máximo 500</span> caracteres
					</div>
				</div>

				<!-- Link -->
				<div class="form-group">
					<label>Link*</label>
					<input class="form-control" type="text" placeholder="Link" name="url" value="<?php  echo $notaImprensa->getUrl() ?>">
				</div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem Principal <i>(Dimensões recomendadas: 965x400)</i></label>
						<input type="file" name="imagem1" id="imagem1">

						<input type="hidden" value="<?php  echo $notaImprensa->getImagem() ?>" name="imagem1-old">

						<?php 

							if ($imagem1 != "") {
								echo "

									<div class='thumb' style='background: url($imagem1) no-repeat center; background-size: cover;'>
					                </div>
					                <br>
		                		";
							}
						?>

					</div>

				</div>
				<a href='index.php?pg=notas-imprensa'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Notas da Imprensa
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Nota da Imprensa" id="botao">
	            <br><br>
			</div>


		</form>
	</div>
</div>