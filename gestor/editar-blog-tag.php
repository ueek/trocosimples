<?php
	
	    //Arquivos externos
    include_once '../models/blog-tag.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $tag = new BlogTag($db);

    // Buscar dados do tag
    $stmtTag = $tag->readById($_GET['id']);


    $urlImagens = "uploads/tags/"; 

    if ($stmtTag->rowCount() > 0) {
	    $row = $stmtTag->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$tag->setId($id);
		$tag->setNome($nome);
        $imagem1 = $urlImagens.$imagem;
				    
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tag</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-blog-tag" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $tag->getId() ?>" name="blog-tag-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Tag -->
				<div class="form-group">
					<label>Tag*</label>
					<input class="form-control" type="text" placeholder="Tag" name="blog-tag" value="<?php  echo $tag->getNome() ?>">
				</div>

				<a href='index.php?pg=blog-tags'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Tags
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Tag" id="botao">
	            
			</div>


		</form>
	</div>
</div>