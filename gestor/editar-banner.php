<?php
	
	    //Arquivos externos
    include_once '../models/banner.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $banner = new Banner($db);

    // Buscar dados do banner
    $stmtBanner = $banner->readById($_GET['id']);


    $urlImagens = "uploads/banners/"; 

    if ($stmtBanner->rowCount() > 0) {
	    $row = $stmtBanner->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$banner->setId($id);
		$banner->setTitulo($titulo);
		$banner->setSubtitulo($subtitulo);
		$banner->setTexto($texto);
		$banner->setUrl($url);
		$banner->setImagem($imagem);
        $imagem1 = $urlImagens.$imagem;
				    
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Banner</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-banner" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $banner->getId() ?>" name="banner-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<div id="imagens-upload">
					<div class="form-group">
						<!-- Título -->
						<div class="form-group">
							<label>Título*</label>
							<input class="form-control" type="text" placeholder="Título" name="titulo" value="<?php  echo $banner->getTitulo() ?>">
						</div>

						<!-- Subtítulo -->
						<div class="form-group">
							<label>Subtítulo*</label>
							<input class="form-control" type="text" placeholder="Subtítulo" name="subtitulo"value="<?php  echo $banner->getSubtitulo() ?>">
						</div>

						<!-- Texto do Botão -->
						<div class="form-group">
							<label>Texto do Botão*</label>
							<input class="form-control" type="text" placeholder="Texto do Botão" name="texto"value="<?php  echo $banner->getTexto() ?>">
						</div>

						<!-- Link (URL) -->
						<div class="form-group">
							<label>Link (URL)*</label>
							<input class="form-control" type="text" placeholder="Link (URL)" name="url"value="<?php  echo $banner->getUrl() ?>">
						</div>

						<!-- Imagem -->
						<label>Imagem Principal <i>(Dimensões recomendadas: 1920x965)</i></label>
						<input type="file" name="imagem1" id="imagem1">

						<input type="hidden" value="<?php  echo $banner->getImagem() ?>" name="imagem1-old">

						<?php 

							if ($imagem1 != "") {
								echo "

									<div class='thumb' style='background: url($imagem1) no-repeat center; background-size: cover;'>
					                </div>
					                <br>
		                		";
							}
						?>

					</div>

				</div>
				<a href='index.php?pg=banners'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Banners
					</button>
				</a>
				

	            <input type="submit" class="btn btn-success direita" value="Alterar Banner" id="botao">
	            <br><br>
			</div>


		</form>
	</div>
</div>