<?php
	
	    //Arquivos externos
    include_once '../models/campanha.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myCampanha = new Campanha($db);

    // Buscar dados do myCampanha
    $stmtCampanha = $myCampanha->readById($_GET['id']);


    $urlImagens = "uploads/campanhas/"; 

    if ($stmtCampanha->rowCount() > 0) {
	    $row = $stmtCampanha->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myCampanha->setId($id);
		$myCampanha->setTitulo($titulo);
		$myCampanha->setResumo($resumo);
		$myCampanha->setUrl($url);
		$myCampanha->setImagem($imagem);
        $imagem1 = $urlImagens.$imagem;
    }

?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Campanha</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-campanha" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $myCampanha->getId() ?>" name="campanha-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Título -->
				<div class="form-group">
					<label>Título*</label>
					<input class="form-control" type="text" placeholder="Título" name="titulo" value="<?php  echo $myCampanha->getTitulo() ?>">
				</div>

				<!-- Resumo -->
				<div class="form-group">
					<label>Resumo</label>
					<textarea class="form-control" name="resumo" placeholder="Resumo" maxlength="500"><?php  echo $myCampanha->getResumo() ?></textarea>
					<div class="restantes">
						<span id="caracteres-restantes">Máximo 500</span> caracteres
					</div>
				</div>

				<!-- Link -->
				<div class="form-group">
					<label>Link*</label>
					<input class="form-control" type="text" placeholder="Link" name="url" value="<?php  echo $myCampanha->getUrl() ?>">
				</div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem Principal</label>
						<input type="file" name="imagem1" id="imagem1">

						<input type="hidden" value="<?php  echo $myCampanha->getImagem() ?>" name="imagem1-old">

						<?php 

							if ($imagem1 != "") {
								echo "

									<div class='thumb' style='background: url($imagem1) no-repeat center; background-size: cover;'>
					                </div>
					                <br>
		                		";
							}
						?>

					</div>

				</div>
				<a href='index.php?pg=campanhas'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Campanhas
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Campanha" id="botao">
	            
			</div>


		</form>
	</div>
</div>