<?php
	
	    //Arquivos externos
    include_once '../models/cliente.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $cliente = new Cliente($db);

    // Buscar dados do cliente
    $stmtCliente = $cliente->readById($_GET['id']);


    $urlImagens = "uploads/clientes/"; 

    if ($stmtCliente->rowCount() > 0) {
	    $row = $stmtCliente->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$cliente->setId($id);
		$cliente->setNome($nome);
		$cliente->setImagem($imagem);
        $imagem1 = $urlImagens.$imagem;
				    
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cliente</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="editar-cliente" enctype="multipart/form-data">

			<input type="hidden" value="<?php  echo $cliente->getId() ?>" name="cliente-id">

			<img src="img/loading.gif" id="carregando">
			
			<div id="fields">

				<!-- Cliente -->
				<div class="form-group">
					<label>Cliente*</label>
					<input class="form-control" type="text" placeholder="Cliente" name="cliente" value="<?php  echo $cliente->getNome() ?>">
				</div>

				<div id="imagens-upload">
					<div class="form-group">
						<label>Imagem Principal</label>
						<input type="file" name="imagem1" id="imagem1">

						<input type="hidden" value="<?php  echo $cliente->getImagem() ?>" name="imagem1-old">

						<?php 

							if ($imagem1 != "") {
								echo "

									<div class='thumb' style='background: url($imagem1) no-repeat center; background-size: cover;'>
					                </div>
					                <br>
		                		";
							}
						?>

					</div>

				</div>
				<a href='index.php?pg=clientes'>
					<button type='button' class='btn btn-info'>
						<i class='fa fa-chevron-left'></i>
						Voltar para Clientes
					</button>
				</a>


	            <input type="submit" class="btn btn-success direita" value="Alterar Cliente" id="botao">
	            
			</div>


		</form>
	</div>
</div>