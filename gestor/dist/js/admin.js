$(document).ready(function() {
    $('.tabela-lobee').DataTable({
        columnDefs: [{
            targets: [2, 3, 5], // column or columns numbers
            orderable: false,  // set orderable for selected columns
        }],
        responsive: true
    });
});