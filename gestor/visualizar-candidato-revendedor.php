<?php
	
    //Arquivos externos
    include_once '../models/revendedor-candidato.php';
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myCandidato = new RevendedorCandidato($db);

    // Buscar dados do candidato
    $stmtCandidato = $myCandidato->readById($_GET['id']);


    if ($stmtCandidato->rowCount() > 0) {
	    $row = $stmtCandidato->fetch(PDO::FETCH_ASSOC);
		extract($row);

		$myCandidato->setId($id);
		$myCandidato->setNome($nome);
		$myCandidato->setCidade($cidade);
		$myCandidato->setEmail($email);
		$myCandidato->setMensagem($mensagem);
		$myCandidato->setSobrenome($sobrenome);
		$myCandidato->setFone($fone);
		$myCandidato->setCurriculo($curriculo);
				    
    }



?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Revendedor - Candidato</h1>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		
		<div class="panel panel-default">
		    <div class="panel-heading">
		        Dados do Candidato
		    </div>
		    <div class="panel-body">

		        <p><strong>Nome: </strong><?php  echo $myCandidato->getNome() ?></p>
		        <p><strong>Sobrenome: </strong><?php  echo $myCandidato->getSobrenome() ?></p>
		        <p><strong>Cidade/UF: </strong><?php  echo $myCandidato->getCidade() ?></p>
		        <p><strong>Fone: </strong><?php  echo $myCandidato->getFone() ?></p>
		        <p><strong>Email: </strong><?php  echo $myCandidato->getEmail() ?></p>
		        <p><strong>Mensagem: </strong><?php  echo $myCandidato->getMensagem() ?></p>

		        <?php  
			        if ($myCandidato->getCurriculo() != null) {
			        	echo 
			        	"
	    					<a target='_blank' href='gestor/uploads/curriculos/{$myCandidato->getCurriculo()}'>
								<button type='button' class='btn btn-success direita'>
									<i class='fa fa-eye'></i>
									Visualizar currículo
								</button>
							</a>
			        	"; 		   
			        }
		        ?>

		    </div>
		</div>

		<a href='index.php?pg=revendedores-candidatos'>
			<button type='button' class='btn btn-info'>
				<i class='fa fa-chevron-left'></i>
				Voltar para Candidatos
			</button>
		</a>

		<?php  
			echo "
				<a href='index.php?pg=cadastrar-revendedor&id_candidato={$myCandidato->getId()}'>
					<button type='button' class='btn btn-success direita'>
						<i class='fa fa-external-link'></i>
						Cadastrar como Revendedor
					</button>
				</a>
			";
		?>

		<br><br>

	</div>
</div>