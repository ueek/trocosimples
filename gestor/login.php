<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta name="author" content="Ueek Ag">
    <link rel="shortcut icon" href="../img/logo-min.png">

    <title>Gestor - Troco Simples</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/css/admin.css" rel="stylesheet">
    <link href="vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <div class="login-panel panel panel-default">
                    <center>
                        <img src="img/logo.png" id="marca-login">
                    </center>
                    <div class="panel-body">
                        <form role="form" id="form-login">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuario" name="login" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="senha" type="password">
                                </div>
                                <input type="submit" class="btn btn-warning" style="width:100%" value="Entrar">
                            </fieldset>

                            <br>
                            <div class="alert alert-danger alert-dismissable" id="erro">
                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                               Usuário ou senha incorretos, <strong>tente novamente</strong>.
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>

    <script src="js/login.js"></script>
</body>
</html>