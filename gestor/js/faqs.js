$(document).ready(function(){
    //Cadastrar 
    $("form#cadastrar-faq").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="titulo"]', form).val() == ""){
            $('input[name="titulo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="texto"]', form).val() == ""){
            $('textarea[name="texto"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('titulo', $('input[name=titulo]').val());
        valores.append('texto', $('textarea[name=texto]').val());

        $.ajax({
            url: "dados/faqs/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=faqs";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            }
        });
    });

    $("form#editar-faq").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="titulo"]', form).val() == ""){
            $('input[name="titulo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="texto"]', form).val() == ""){
            $('textarea[name="texto"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('faq-id', $('input[name=faq-id]').val());
        valores.append('titulo', $('input[name=titulo]').val());
        valores.append('texto', $('textarea[name=texto]').val());

        $.ajax({
            url: "dados/faqs/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=faqs";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remover-faq" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este item? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/faqs/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});