$(document).ready(function(){
    //Cadastrar 
    $("form#cadastrar-cliente").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="cliente"]', form).val() == ""){
            $('input[name="cliente"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("imagem1").files.length == 0) {
            $('input[name="imagem1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('cliente', $('input[name=cliente]').val());
        valores.append('imagem1', $("#imagem1")[0].files[0]);

        $.ajax({
            url: "dados/clientes/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=clientes";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            }
        });
    });

    $("form#editar-cliente").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="cliente"]', form).val() == ""){
            $('input[name="cliente"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('cliente-id', $('input[name=cliente-id]').val());
        valores.append('cliente', $('input[name=cliente]').val());

        if(document.getElementById("imagem1").value != "") {
            valores.append('imagem1', $("#imagem1")[0].files[0]);
        }else{
            valores.append('imagem1-old', $('input[name=imagem1-old]').val());
        }

        $.ajax({
            url: "dados/clientes/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=clientes";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remover-cliente" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este item? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/clientes/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});