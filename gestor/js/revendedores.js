$(document).ready(function(){
    //Cadastrar 
    $("form#cadastrar-revendedor").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="nome"]', form).val().trim() == ""){
            $('input[name="nome"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="cidade-uf"]', form).val().trim() == ""){
            $('input[name="cidade-uf"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="fone"]', form).val().trim() == ""){
            $('input[name="fone"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="email"]', form).val().trim() == ""){
            $('input[name="email"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }                        



        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('nome', $('input[name=nome]').val());
        valores.append('cidade-uf', $('input[name=cidade-uf]').val());
        valores.append('fone', $('input[name=fone]').val());
        valores.append('email', $('input[name=email]').val());

        $.ajax({
            url: "dados/revendedores/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=revendedores";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("erro");
            }
        });
    });

    $("form#editar-revendedor").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="nome"]', form).val().trim() == ""){
            $('input[name="nome"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="cidade-uf"]', form).val().trim() == ""){
            $('input[name="cidade-uf"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="fone"]', form).val().trim() == ""){
            $('input[name="fone"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="email"]', form).val().trim() == ""){
            $('input[name="email"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('revendedor-id', $('input[name=revendedor-id]').val());
        valores.append('nome', $('input[name=nome]').val());
        valores.append('cidade-uf', $('input[name=cidade-uf]').val());
        valores.append('fone', $('input[name=fone]').val());
        valores.append('email', $('input[name=email]').val());

        $.ajax({
            url: "dados/revendedores/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=revendedores";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remover-revendedor" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este item? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/revendedores/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});