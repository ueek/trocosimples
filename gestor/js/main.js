$(document).ready(function() {
		//DataTable
		$('#dataTables-example').DataTable({
						responsive: true
		});

		//Fancy
  		$('.fancybox').fancybox();

});

// tooltip demo
$('.tooltip-demo').tooltip({
		selector: "[data-toggle=tooltip]",
		container: "body"
});

// popover demo
$("[data-toggle=popover]").popover();

//AprovadasReprovadas Home
var aprv = $('#aprovadas-dia').text();
var aprv = parseFloat(aprv);

var repr = $('#reprovadas-dia').text();
var repr = parseFloat(repr);

//Morris
if(aprv != 0 || repr != 0){
	new Morris.Donut({
		element: 'comparativo',
		data: [
			{label: "Aprovadas", value: aprv},
			{label: "Reprovadas", value: repr}
		],
		colors: ['#5cb85c', '#d9534f']
	});
}else{
	$("#comparativo").html("Nenhuma vistoria realizada ainda.");
}