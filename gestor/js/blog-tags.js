$(document).ready(function(){
    //Cadastrar 
    $("form#cadastrar-blog-tag").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="blog-tag"]', form).val() == ""){
            $('input[name="blog-tag"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('blog-tag', $('input[name=blog-tag]').val());

        $.ajax({
            url: "dados/blog-tags/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=blog-tags";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            }
        });
    });

    $("form#editar-blog-tag").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="blog-tag"]', form).val() == ""){
            $('input[name="blog-tag"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('blog-tag-id', $('input[name=blog-tag-id]').val());
        valores.append('blog-tag', $('input[name=blog-tag]').val());

        $.ajax({
            url: "dados/blog-tags/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=blog-tags";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remover-blog-tag" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este item? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/blog-tags/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});