$(document).ready(function(){

    $('#texto').summernote(
        {
            height: 400,
            toolbar: [
              ['style', ['style']],
              ['font', ['bold', 'underline', 'clear']],
              ['fontname', ['fontname']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['insert', ['link', 'video']],
              ['view', ['fullscreen', 'codeview', 'help']],
            ],
        }
    );

    //Cadastrar 
    $('form#cadastrar-blog-post').submit(function(e){
        e.preventDefault();

        var form = $(this);

        $('input[type="text"], textarea', form).css({
            '-webkit-box-shadow':'none',
            'box-shadow':'none'
        });

        if($('input[name="titulo"]', form).val() == ""){
            $('input[name="titulo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="texto"]', form).val() == ""){
            $('textarea[name="texto"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        var valores = form.serialize();
        
        $('input', form).attr('disabled', 'disabled');
        $('textarea', form).attr('disabled', 'disabled');

        $.ajax({
            type     : 'POST',
            url      : 'dados/blog-posts/create.php',
            data     : valores,
            dataType : 'json',
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');
                if(retorno.status == 1){
                    alert(retorno.msg);
                    window.location.href="index.php?pg=blog-posts";
                }else{
                    alert(retorno.msg);
                }
            }
        });
    });

    $('form#editar-blog-post').submit(function(e){
        e.preventDefault();

        var form = $(this);

        $('input[type="text"], textarea', form).css({
            '-webkit-box-shadow':'none',
            'box-shadow':'none'
        });

        if($('input[name="titulo"]', form).val() == ""){
            $('input[name="titulo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="texto"]', form).val() == ""){
            $('textarea[name="texto"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        var valores = form.serialize();
        
        $('input', form).attr('disabled', 'disabled');
        $('textarea', form).attr('disabled', 'disabled');

        $.ajax({
            type     : 'POST',
            url      : 'dados/blog-posts/update.php',
            data     : valores,
            dataType : 'json',
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');
                if(retorno.status == 1){
                    alert(retorno.msg);
                    window.location.href="index.php?pg=blog-posts";
                }else{
                    alert(retorno.msg);
                }
            }
        });
    });

    //Cadastrar IMAGENS 
    $("form#imagens-blog-post").submit(function(e) {
        e.preventDefault();

        var data = new FormData(this);

        //Loading
        $('#imagens-upload').hide();
        $('#carregando').show();

        $.ajax({
            url: "dados/blog-posts/update-imagem.php",
            type: "POST",
            data: data,
            mimeType: "multipart/form-data",
            dataType : 'json',
            contentType: false,
            cache: false,
            processData:false,
            success: function (data) {
                if(data.status == 1){
                    alert(data.msg);
                    window.location.href="index.php?pg=blog-posts";
                }else{
                    $('#carregando').hide();
                    $('#imagens-upload').show();
                    alert(data.msg);
                }
            }
        });
    });

    
    //Remover 
    $( ".remover-blog-post" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este blog-post? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type    : 'POST',
                url     : 'dados/blog-posts/delete.php',
                data    : {id : id},
                dataType : 'json',
                success : function(data){
                    if(data.status == 1){
                        alert(data.msg);
                        window.location.href="index.php?pg=blog-posts";
                    }else{
                        alert(data.msg);
                    }
                }
            });
        }
    });
});