$(document).ready(function(){

	// Login Gestor
	var form = $('form[id="form-login"]');

	form.submit(function(e){
		e.preventDefault();

		var valores = form.serialize();
		$.ajax({
			type: 'POST',
			url: 'logar.php',
			data: valores,
			success: function(retorno){
				if(retorno == 1){
					$('#erro').hide();
					location.href='index.php';
				}else{
					//alert(retorno);
					$('#erro').show();
				}
			}
		});
	});

});