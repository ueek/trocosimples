$(document).ready(function(){
    //Cadastrar 
    $("form#cadastrar-sistema").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="sistema"]', form).val() == ""){
            $('input[name="sistema"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("imagem1").files.length == 0) {
            $('input[name="imagem1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('sistema', $('input[name=sistema]').val());
        valores.append('tipo', $('select[name=tipo]').val());
        valores.append('imagem1', $("#imagem1")[0].files[0]);

        $.ajax({
            url: "dados/sistemas/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=sistemas";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            }
        });
    });

    $("form#editar-sistema").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="sistema"]', form).val() == ""){
            $('input[name="sistema"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('sistema-id', $('input[name=sistema-id]').val());
        valores.append('sistema', $('input[name=sistema]').val());
        valores.append('tipo', $('select[name=tipo]').val());

        if(document.getElementById("imagem1").value != "") {
            valores.append('imagem1', $("#imagem1")[0].files[0]);
        }else{
            valores.append('imagem1-old', $('input[name=imagem1-old]').val());
        }

        $.ajax({
            url: "dados/sistemas/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=sistemas";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remover-sistema" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este item? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/sistemas/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});