$(document).ready(function(){
    //Cadastrar IMAGENS PRODUTO
    $("form#imagens-blog-post").submit(function(e) {
        e.preventDefault();

        var data = new FormData(this);

        //Loading
        $('#imagens-upload').hide();
        $('#carregando').show();

        $.ajax({
            url: "dados/fotos-blog-posts/create.php",
            type: "POST",
            data: data,
            mimeType: "multipart/form-data",
            dataType : 'json',
            contentType: false,
            cache: false,
            processData:false,
            success: function (data) {
            	if(data.status == 1){
    				alert(data.msg);
    				window.location.href="index.php?pg=blog-posts";
    			}else{
        			$('#carregando').hide();
        			$('#imagens-upload').show();
    				alert(data.msg);
    			}
            },
            error: function (data) {
                alert("error");
            }
        });
    });

    //Remover IMAGENS 
    $( ".remover-foto-blog-post" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover esta imagem? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/fotos-blog-posts/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});