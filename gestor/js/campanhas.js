$(document).ready(function(){
    //Cadastrar 
    $("form#cadastrar-campanha").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="titulo"]', form).val() == ""){
            $('input[name="titulo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="resumo"]', form).val() == ""){
            $('textarea[name="resumo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="url"]', form).val() == ""){
            $('input[name="url"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("imagem1").files.length == 0) {
            $('input[name="imagem1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('titulo', $('input[name=titulo]').val());
        valores.append('resumo', $('textarea[name=resumo]').val());
        valores.append('url', $('input[name=url]').val());
        valores.append('imagem1', $("#imagem1")[0].files[0]);

        $.ajax({
            url: "dados/campanhas/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=campanhas";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            }
        });
    });

    $("form#editar-campanha").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="titulo"]', form).val() == ""){
            $('input[name="titulo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="resumo"]', form).val() == ""){
            $('textarea[name="resumo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="url"]', form).val() == ""){
            $('input[name="url"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('campanha-id', $('input[name=campanha-id]').val());
        valores.append('titulo', $('input[name=titulo]').val());
        valores.append('resumo', $('textarea[name=resumo]').val());
        valores.append('url', $('input[name=url]').val());

        if(document.getElementById("imagem1").value != "") {
            valores.append('imagem1', $("#imagem1")[0].files[0]);
        }else{
            valores.append('imagem1-old', $('input[name=imagem1-old]').val());
        }

        $.ajax({
            url: "dados/campanhas/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    alert(json.msg);
                    window.location.href="index.php?pg=campanhas";
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    alert(json.msg);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remover-campanha" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover este item? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'dados/campanhas/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });
});