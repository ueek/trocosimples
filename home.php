<?php
    include_once 'models/banner.php';

	// Instanciar objetos
	$banner = new Banner($db);
	$myBanners = $banner->read();
?>

<div class="regular1 margin-banner">

	<!--  -->
	<?php 

		while ($row = $myBanners->fetch(PDO::FETCH_ASSOC)){
			extract($row);

			$urlImagem = "gestor/uploads/banners/".$imagem; 

			echo"
				<div>
			      	<section class='banner' style='background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url($urlImagem); background-repeat: no-repeat;
		background-position: center;'>
						<div class='conteudo'>
							
							<h1>$titulo</h1>

							<span>$subtitulo</span>

							<a href='$url'>
								<div class='btn-banner soft-hover'>$texto</div>
							</a>

						</div>
					</section>
			    </div>
			";
			
		}

	?>
</div>

<section class="sep-banner"></section>

<section id="jeito-simples">
	<div class="conteudo">
		<h1>Um jeito simples e moderno de dar e receber troco.</h1>

		<p>A melhor forma de acabar com o problema da falta de moedas.<br> Como e por que usar?</p>

		<div id="itens-jeito" class="">
			<a href="/para-voce">
				<div class="item-jeito soft-hover">
					<img src="img/para-voce.png" alt="TrocoSimples">

					<h2>Para Você</h2>
				</div>
			</a>
			<a href="/para-empresas">
				<div class="item-jeito soft-hover">
					<img src="img/para-empresas.png" alt="TrocoSimples">

					<h2>Para Empresas</h2>
				</div>
			</a>
			<a href="/fale-conosco">
				<div class="item-jeito soft-hover">
					<img src="img/contate-nos.png" alt="TrocoSimples">

					<h2>Contate-nos</h2>
				</div>
			</a>
		</div>
	</div>
</section>

<section id="para-voce">
	<div class="conteudo">
		<h1>PARA VOCÊ</h1>

		<h2>Não perca os seus trocados por aí</h2>

		<p>Chega de troco em bala! Aceite o troco digital pelo seu CPF e use como quiser:</p>

		<div id="itens-paravoce" class="">
			<div class="item-paravoce">
				<img src="img/transferencia-automatica.png" alt="TrocoSimples">

				<h3>Transf. automática <br>para conta bancária</h3>
			</div>
			<div class="item-paravoce">
				<img src="img/inserir-creditos.png" alt="TrocoSimples">

				<h3>Inserir créditos <br>no celular</h3>
			</div>
			<div class="item-paravoce">
				<img src="img/pagar-conta.png" alt="TrocoSimples">

				<h3>Pagar a sua conta <br>na rede de parceiros</h3>
			</div>
			<div class="item-paravoce">
				<img src="img/rendimentos-app.png" alt="TrocoSimples">

				<h3>Rendimentos <br>no aplicativo</h3>
			</div>
			<div class="item-paravoce">
				<img src="img/enviar-dinheiro.png" alt="TrocoSimples">

				<h3>Enviar dinheiro <br>para um amigo</h3>
			</div>
			<br>
		</div>

		<div id="itens-loja" class="">
			<a target="_blank" href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8&utm_source=site&utm_medium=botao&utm_content=ios">
				<img src="img/ios.png" alt="TrocoSimples">
			</a>
			<a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor&utm_source=site&utm_medium=botao&utm_content=android">
				<img src="img/android.png" alt="TrocoSimples">
			</a>
			<br>
		</div>

	</div>
</section>

<section class="formularios">
	<div class="conteudo">
		<h1>PARA EMPRESAS</h1>

		<h2>Saia do prejuízo pela falta de moedas</h2>

		<p>
			Rodamos integrados ao seu frente de caixa (PDV) e ou TEF,  para que você entregue troco digital direto no CPF dos seus clientes.
		</p>

		<form class="form-para-empresas formulario">
			<div class="col-1">
				
				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Responsável*</label>
				<input class="soft-hover" type="text" name="responsavel" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

			</div>
			<div class="col-2">
				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>Estado*</label>
				<input class="soft-hover" type="text" name="estado" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>

<section id="usamos-tecnologia">
	<div class="conteudo">
		<h1 class="">Nós usamos a tecnologia para mudar a forma como varejistas e consumidores trabalham com o dinheiro.</h1>

		<a href="/sobre">
			<div id="btn-usamos" class="soft-hover">Conheça a Troco Simples</div>
		</a>
	</div>
</section>

<section class="clientes-parceiros">
	<div class="conteudo">
		<h1>Clientes & Parceiros</h1>

		<span>Conheça alguns dos clientes parceiros da Troco Simples</span>

		<div class="itens-clientesparceiros regular5">

			<?php 

				while ($row = $myClientes->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					$urlImagem = "gestor/uploads/clientes/".$imagem; 

					echo"
						<div>
					      	<div class='item-clientesparceiros' style='background: url($urlImagem); background-position: center; background-repeat: no-repeat; background-size: contain;'>
					      	</div>
					    </div>
					";
					
				}

			?>

			<br>
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->