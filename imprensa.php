<?php
    include_once 'models/nota-imprensa.php';

	// Instanciar objetos
	$imprensa = new NotaImprensa($db);
	$myNotasImprensa = $imprensa->read("id DESC");

	$countDados = $myNotasImprensa->rowCount();
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-imprensa.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section class="publicacoes">
	<div class="conteudo">
		<h1>Imprensa</h1>

		<p class="desc-publicacoes">
			Uma prova de que a Troco Simples faz a diferença para a sociedade é que a mídia está falando sobre isso.<br>
			Acompanhe aqui o que está sendo falado sobre o sucesso do nosso trabalho:
		</p>

		<div class="itens-publicacoes">

			<?php 

				if($countDados <> 0){
					$itensPagina = 10;
				    $inicio = calcula_inicio_limite_query($itensPagina);
					
	                
				
				    //$myNotasImprensa = $imprensa->read();
					$myNotasImprensa = $imprensa->read("id DESC", "LIMIT {$inicio}, {$itensPagina}");

				    while ($row = $myNotasImprensa->fetch(PDO::FETCH_ASSOC)){
						extract($row);

						$urlImagem = "gestor/uploads/notas-imprensa/".$imagem; 
						$tituloTruncado = substr($titulo, 0, 50)."...";
						$resumo = htmlspecialchars_decode($resumo);
						$resumoTruncado = substr($resumo, 0, 100)."...";

						echo"
							<div class='item-publicacoes'>
								<div class='img-publicacoes' style='background: url($urlImagem); background-repeat: no-repeat; background-position: center; background-size: cover;'></div>

								<h2>$tituloTruncado</h2>

								<p>$resumoTruncado</p>

								<a target='_blank' href='$url'>
									<span class='soft-hover'>Ver mais</span>
								</a>
							</div>
						";
						
					}


					if(isset($_GET['linha']) and !empty($_GET['linha'])){
						paginacao($countDados, $itensPagina, 5, "pag");
					}else{
						paginacao($countDados, $itensPagina, 5, "pag");
					}
				}else{
					echo "Nenhum produto encontrado.";
				}

			?>
			<br>
		</div>
	</div>
</section>

<section class="newsletter">
	<div class="conteudo">
		<h1>Newsletter</h1>

		<p>Quer receber nossas informações por e-mail? Então assine nossa Newsletter.</p>

		<form id="form-newsletter" class="form-newsletter">

			<div class="center-newsletter">
				<div class="campo-newsletter">
					<label>Nome*</label>
					<input class="soft-hover" type="text" name="nome">

					<br>
					<br>
				</div>

				<div class="campo-newsletter">
					<label>E-mail*:</label>
					<input class="soft-hover" type="text" name="email">				
				</div>
				<br>
			</div>

			<input class="soft-hover" type="submit" value="Enviar">

			<span class="">Campos marcados por um * são obrigatórios.</span>
		</form>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

		
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->