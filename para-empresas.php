<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-paraempresas.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="para-empresas">
	<div class="conteudo">

		<img id="right-parempresas" src="img/img-paraempresas.png" alt="TrocoSimples">

		<div id="left-paraempresas">
			<h1>Acabe com a falta de troco e mantenha seu negócio lucrativo!</h1>

			<h2>Inove na forma de entregar o troco e reduza custos!</h2>

			<p class="">
				A Troco SImples facilita as transações financeiras do varejo com troco digital diretamente no CPF do cliente e ainda reduz seus custos com aquisição e manuseio de moedas.
			</p>
		</div>
		<br>
	</div>
</section>

<section class="formularios" id="formulario">
	<div class="conteudo">
		<h1>PARA EMPRESAS</h1>

		<h2>Saia do prejuízo pela falta de moedas</h2>

		<p>
			Rodamos integrados ao seu frente de caixa (PDV) e ou TEF,  para que você entregue troco digital direto no CPF dos seus clientes.
		</p>

		<form class="form-para-empresas formulario">
			<div class="col-1">
				
				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Responsável*</label>
				<input class="soft-hover" type="text" name="responsavel" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

			</div>
			<div class="col-2">
				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>Estado*</label>
				<input class="soft-hover" type="text" name="estado" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>

<section id="simplifique-vantagens">
	<div class="conteudo">
		<div id="col-1" class="">
			<h1>Simplifique a forma como seu comércio trabalha com troco</h1>

			<p>
				A Troco Simples simplifica as transações financeiras do seu negócio que envolvem dinheiro em espécie, trazendo uma opção fácil, acessível e barata de entregar o troco para seus clientes. Quer saber como? Com troco digital!
			</p>
		</div>
		<div id="col-2" class="">
			<h1>Vantagens de usar o troco digital no seu comércio</h1>

			<ul>
				<div class="item-simplifique">
					<li style="background: url(/img/1.png); background-position: left; background-repeat: no-repeat;">Acaba com o problema da falta de moeda para troco;</li>
					<li style="background: url(/img/2.png); background-position: left; background-repeat: no-repeat;">Reduz a quebra de caixa;</li>
					<li style="background: url(/img/3.png); background-position: left; background-repeat: no-repeat;">Fim da correria na busca por moedas;</li>					
				</div>
				<div class="item-simplifique">
					<li style="background: url(/img/4.png); background-position: left; background-repeat: no-repeat;">Diminui o custo de transporte de valores;</li>
					<li style="background: url(/img/5.png); background-position: left; background-repeat: no-repeat;">Fácil de contratar e de operar;</li>
					<li style="background: url(/img/6.png); background-position: left; background-repeat: no-repeat;">Integração com os principais frentes de caixa (PDV) e Tef do mercado.</li>
				</div>
				<br>
			</ul>
		</div>
		<br>
	</div>
</section>

<section id="quero-fazer">
	<div class="conteudo">
		<h1>Quero fazer parte da rede de parceiros!</h1>

		<h2>Como funciona?</h2>

		<p>
			A Troco Simples trabalha criando redes com varejistas para transformar o dinheiro físico em troco digital através de integrações e benefícios, para acabar com o problema da falta de moedas.
		</p>

		<div id="primeiros-itens" class="itens-querofazer">
			<div class="item-querofazer">
				<img src="img/faca-cadastro.png" alt="TrocoSimples">

				<h3>1.</h3>

				<span>Faça o cadastro da sua empresa através do nosso site;</span>
			</div>
			<div class="item-querofazer">
				<img src="img/carregue-reserva.png" alt="TrocoSimples">

				<h3>2.</h3>

				<span>Carregue sua reserva de troco digital de forma pré-paga;</span>
			</div>
			<div class="item-querofazer">
				<img src="img/vendas-dinheiro.png" alt="TrocoSimples">

				<h3>3.</h3>

				<span>Nas vendas em dinheiro ofereça a devolução em troco digital no CPF do cliente;</span>
			</div>
		</div>
		<br>
		<div class="itens-querofazer">
			<div class="item-querofazer">
				<img src="img/valor-debitado.png" alt="TrocoSimples">

				<h3>4.</h3>

				<span>O valor é debitado da reserva de troco digital;</span>
			</div>
			<div class="item-querofazer">
				<img src="img/dinheiro-creditado.png" alt="TrocoSimples">

				<h3>5.</h3>

				<span>O dinheiro é creditado no CPF do cliente e ele não precisa se cadastrar.</span>
			</div>
		</div>
		<br>

		<a href="/para-empresas#formulario">
			<div id="btn-querofazer" class="soft-hover">Quero começar agora</div>
		</a>
	</div>
</section>

<section id="ainda-esta">

	<div id="ainda-esta-left"></div>

	<div id="ainda-esta-right">
		
		<div id="ainda-esta-info">
			<h1>Ainda está na dúvida se a Troco Simples é a solução para o seu negócio?</h1>

			<p>
				Entregar troco digital com a Troco Simples é muito fácil, pois funciona diretamente na sua Frente de Caixa (PDV) e ou TEF.
			</p>

			<a href="/sistemas">
				<div id="btn-aindaesta" class="soft-hover">Conhecer sistemas integrados</div>
			</a>
		</div>

	</div>
	<br>
</section>

<section id="vantagens-troco">
	<div class="conteudo">
		<h1>Vantagens do troco digital para os seus clientes.</h1>

		<ul class="">
			<li>Receber troco digital direto no CPF;</li><br>
			<li>Transferência automática e gratuita do troco digital para a conta bancária;</li><br>
			<li>Usar o saldo acumulado para usar na rede de parceiros Troco Simples;</li><br>
			<li>Recarregar o crédito pré-pago no celular com o troco digital acumulado;</li><br>
			<li>Render o troco digital acumulando na Troco Simples em até 6% a.a.</li>
		</ul>

		<a href="/para-empresas-saiba-mais">
			<div id="btn-vantagenstroco" class="soft-hover">Saiba Mais</div>
		</a>
	</div>
</section>

<section class="clientes-parceiros">
	<div class="conteudo">
		<h1>Clientes & Parceiros</h1>

		<p class="display-desktop">
			Vamos juntos mudar a forma como os brasileiros se relacionam com o troco. <br>Faça parte da rede Troco Simples e peça seu troco digital.
		</p>

		<span class="display-mobile">Conheça alguns dos clientes parceiros da Troco Simples</span>

		<div class="itens-clientesparceiros regular5">
			<?php 

				while ($row = $myClientes->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					$urlImagem = "gestor/uploads/clientes/".$imagem; 

					echo"
						<div>
					      	<div class='item-clientesparceiros' style='background: url($urlImagem); background-position: center; background-repeat: no-repeat; background-size: contain;'>
					      	</div>
					    </div>
					";
					
				}

			?>

			<br>
		</div>
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->