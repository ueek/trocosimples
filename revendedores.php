<?php
    include_once 'models/revendedor.php';

	// Instanciar objetos
	$revendedor = new Revendedor($db);

	if (isset($_GET['nome']) || isset($_GET['estado'])) {
		$nome_filtro = $_GET['nome'];
		if ($nome_filtro == "todos") {
			$nome_filtro = "";
		}

		$estado_filtro = $_GET['estado'];
		if ($estado_filtro == "todos") {
			$estado_filtro = "";
		}
		
		$myRevendedores = $revendedor->read("id DESC", "", "AND nome LIKE '%".$nome_filtro."%' AND cidade_uf LIKE '%".$estado_filtro."%'");	
	}else{
		$myRevendedores = $revendedor->read("id DESC");	
	}

	
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-revendedores.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="revendedores">
	<div class="conteudo">

		<h1>Revendedores</h1>

		<p>
			A Troco Simples quer alcançar todo o Brasil. Por isso, você pode falar diretamente com um de nossos revendedores, basta escolher aquele que está mais próximo de você. Dessa forma você poderá fazer parte dessa rede que apoia e usa o troco digital ainda hoje.
			<br>
			<br>
			Se sua região não possui um revendedor, <a href="/fale-conosco">clique aqui</a> que redirecionaremos você para a nossa equipe comercial. Se você possui perfil comercial e também acredita que com novas ideias podemos resolver problemas antigos, estamos procurando pessoas como você. <a href="/seja-revendedor">Seja um revendedor</a> e faça parte do time da Troco Simples.
		</p>

		<img src="/img/linha-revendedores.png" alt="TrocoSimples">

		<form id="form-revendedores" class="">
			<h2>Nossos representantes:</h2>

			<input id="nome" type="text" name="nome" placeholder="Procurar por nome">

			<label>Selecione aqui o Estado desejado:</label>
			<select id="estado" name="estado">
				<option value="todos">UF</option>
				<option value="AC">AC</option>
				<option value="AL">AL</option>
				<option value="AP">AP</option>
				<option value="AM">AM</option>
				<option value="BA">BA</option>
				<option value="CE">CE</option>
				<option value="DF">DF</option>
				<option value="ES">ES</option>
				<option value="GO">GO</option>
				<option value="MA">MA</option>
				<option value="MT">MT</option>
				<option value="MS">MS</option>
				<option value="MG">MG</option>
				<option value="PA">PA</option>
				<option value="PB">PB</option>
				<option value="PR">PR</option>
				<option value="PE">PE</option>
				<option value="PI">PI</option>
				<option value="RJ">RJ</option>
				<option value="RN">RN</option>
				<option value="RS">RS</option>
				<option value="RO">RO</option>
				<option value="RR">RR</option>
				<option value="SC">SC</option>
				<option value="SP">SP</option>
				<option value="SE">SE</option>
				<option value="TO">TO</option>
			</select>
			
			<br class="display-mobile">
			<br class="display-mobile">
			
			<input class="soft-hover" type="submit" value="Filtrar">
			<br>
		</form>

		<div id="itens-revendedores" class="">

			<?php 

				if ($myRevendedores->rowCount() > 0) {
					while ($row = $myRevendedores->fetch(PDO::FETCH_ASSOC)){
						extract($row);

						$cidade_uf = strtoupper($cidade_uf);

						echo"
							<div class='item-revendedores soft-hover'>
								<h3>$nome</h3>

								<h4>$cidade_uf</h4>

								<span>$fone</span>
								<span>$email</span>
							</div>
						";
						
					}					
				}


			?>
			<div class="item-revendedores item-trocosimples soft-hover">
				<img src="/img/logo-revendedores.png" alt="TrocoSimples">

				<span>41 3514.4930</span>
				<span>contato@trocosimples.com.br</span>
			</div>

			<br>
		</div>

	</div>
</section>


<!--Exibir o formulário caso não tenha nenhum revendedor cadastrado 
<section class="formularios">
	<div class="conteudo">
		<h1>PARA EMPRESAS</h1>

		<h2>Saia do prejuízo pela falta de moedas</h2>

		<p>
			Rodamos integrados ao seu frente de caixa (PDV) e ou TEF,  para que você entregue troco digital direto no CPF dos seus clientes.
		</p>

		<form class="formulario">
			<div class="col-1">
				
				<label>Nome*</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

				<label>Segmento*</label>
				<input class="soft-hover" type="text" name="segmento" placeholder="">

			</div>
			<div class="col-2">
				<label>Último Nome*</label>
				<input class="soft-hover" type="text" name="ultimonome" placeholder="">

				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Número de clientes*</label>
				<input class="soft-hover" type="text" name="numeroclientes" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span>
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="/img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="/img/erro.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

	</div>
</section>-->
<!-- Exibir o formulário caso não tenha nenhum revendedor cadastrado -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->