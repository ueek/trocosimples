<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-revendedores.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="detalhes">
	<div class="conteudo">

		<span><a href="index.php?pg=sistemas"><img src="img/voltar.png" alt="TrocoSimples"></a> Sistemas</span>

		<h1>Detalhes</h1>

		<p>
			A Troco Simples considera a frente de caixa uma ferramenta estratégica para o varejo.<br><br>

			As software house, detentoras dessa tecnologia, podem se tornar grandes integradores de serviços e participar de uma remuneração significativa gerada por eles.<br><br>

			A Troco Simples é um serviço que através de uma comunicação via TEF ou diretamente com a frente de caixa, leva um serviço de alto valor agregado ao varejista, com maior usabilidade para o caixa e baixo impacto de suporte.<br><br>

			Com isso, a oferta da Troco Simples integrada ao seu sistema de PDV, não onera seu suporte técnico e aumenta seu ticket médio por cliente.<br><br>

			Aproveite! Seus clientes anseiam a muito tempo por soluções que se proponham resolver a falta de moedas e pequenas notas (R$ 2,00 e R$ 5,00). Por isso, a Troco Simples está fechando parcerias em todo território nacional em todos os segmentos do varejo.<br><br>

			Com o apoio do seu time de vendas, em menos de 6 meses podemos alcançar 50% da sua carteira de clientes e torná-la usuária da Troco Simples.<br><br>

			Já possuímos parcerias com sistemas de diversos segmentos, para: supermercados, farmácias, postos de combustível e conveniência, materiais de construção, estacionamentos, empresas de ônibus, entre muitos outros. <br><br>

			Se seus clientes utilizam algum dos nossos TEF’s homologados, o esforço de comunicação e autorização para com a Troco Simples é baixíssimo. Se não se utilizam desse tipo de sistema, disponibilizamos uma API  própria para comunicação direta. Não tem como ficar de fora!<br><br>

			Se quiser entender mais sobre o nosso modelo de parceria e integração, preencha o formulário que nosso time de parcerias entrará em contato:<br><br>
		</p>
		
	</div>
</section>

<section class="formularios">
	<div class="conteudo">

		<h2>Sistemas</h2><br><br>

		<form class="formulario">
			<div class="col-1">
				
				<label>Nome*</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

				<label>Segmento*</label>
				<input class="soft-hover" type="text" name="segmento" placeholder="">

			</div>
			<div class="col-2">
				<label>Último Nome*</label>
				<input class="soft-hover" type="text" name="ultimonome" placeholder="">

				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Número de clientes*</label>
				<input class="soft-hover" type="text" name="numeroclientes" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->