-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 03, 2019 at 03:06 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ueekc672_trocosimples`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `senha`, `cadastrado_em`, `alterado_em`, `status`) VALUES
(1, 'suporte@ueek.com.br', '202cb962ac59075b964b07152d234b70', '2019-04-09 15:29:58', '2019-04-09 15:29:58', 1),
(2, 'admin@trocosimples.com.br', 'ca84a7b596ecdd6db7fd65bcea9cea52', '2019-05-06 17:46:29', '2019-05-06 17:46:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `titulo`, `subtitulo`, `texto`, `url`, `imagem`, `cadastrado_em`, `alterado_em`, `status`) VALUES
(13, 'Pare de carregar ou perder moedas.', 'Apenas com seu CPF você já acumula seu troco de forma digital.', 'Veja como é Simples', 'http://trocosimples.cliente.ueek.com.br/para-voce', '77c86b32e2f1774a3df490df5cb62cae.jpg', '2019-05-31 13:41:25', '2019-05-31 14:17:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_categorias`
--

CREATE TABLE `blog_categorias` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(11) NOT NULL,
  `categoria_ids` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_fotos`
--

CREATE TABLE `blog_post_fotos` (
  `id` int(11) NOT NULL,
  `blog_post_id` int(11) NOT NULL,
  `imagem` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campanhas`
--

CREATE TABLE `campanhas` (
  `id` int(11) NOT NULL,
  `imagem` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resumo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='	';

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nome`, `imagem`, `cadastrado_em`, `alterado_em`, `status`) VALUES
(25, 'Cliente legal', '7c2e6d320a201793a97bcb2d18798f28.jpg', '2019-04-11 14:40:31', '2019-04-11 14:40:41', 0),
(26, 'Teste', '25761bf97748fb3314313ae2dbd8d4cb.jpg', '2019-04-16 17:04:04', '2019-04-16 17:04:04', 0),
(27, 'Cliente 1', 'e95a6da1b7ba0b9852dfe8bbb14b7a8d.jpg', '2019-05-04 12:38:32', '2019-05-06 18:08:36', 0),
(28, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(29, 'Cliente 3', 'b593e1239248af49db9e1b74a4a61cc0.jpg', '2019-05-04 12:39:02', '2019-05-06 18:09:02', 0),
(30, 'Cliente 4', '3492bb155a361645d481225d36f6df27.jpg', '2019-05-04 12:46:10', '2019-05-04 12:46:10', 0),
(31, 'Cliente 5', '3369d31cd05aa60ee96d2e822b9ebf10.jpg', '2019-05-04 12:46:19', '2019-05-04 12:46:19', 0),
(32, 'Cliente 6', 'e993b7bc1ba3d6661f0d7b8efed8d8fd.jpg', '2019-05-04 12:46:33', '2019-05-04 12:46:33', 0),
(33, 'Cliente 7', '3992deea85d66341a0846d166c5d30de.jpg', '2019-05-04 12:46:49', '2019-05-04 12:46:49', 0),
(34, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(35, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(36, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(37, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(38, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(39, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(40, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(41, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(42, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(43, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(44, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(45, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(46, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(47, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(48, 'Cliente 2', '66a55cb59b94ba5a1c4c26188f4e872b.jpg', '2019-05-04 12:38:44', '2019-05-06 18:08:49', 0),
(49, 'Superpão', 'fde26d59a6956ae48f31b5166ff2f623.png', '2019-05-14 11:57:36', '2019-05-14 11:57:36', 1),
(50, 'VerdeMais', 'a484e72eb4dbe81620d21db3e5c63197.png', '2019-05-14 11:58:10', '2019-05-14 11:58:10', 1),
(51, 'Sorvetes Bapka', 'a2c3e34bc3d0036994239d92f08db505.png', '2019-05-14 11:58:50', '2019-05-14 11:58:50', 1),
(52, 'Colatusso', '94247f923d273a3c1a5f7d57a3ffb68e.png', '2019-05-14 11:59:18', '2019-05-14 11:59:18', 1),
(53, 'Boni', 'fc2eb1428c2e39cbb5e05644598fce0a.jpg', '2019-05-14 11:59:34', '2019-05-14 11:59:34', 1),
(54, 'Visual Mix', '51f945472fab93b6219bc3d9f61d451d.png', '2019-05-14 13:43:26', '2019-05-14 13:43:26', 0),
(55, 'Avanço', '101f12f6892990fc8e5ec0fef8fce1af.png', '2019-05-14 13:43:59', '2019-05-14 13:43:59', 0),
(56, 'CompoFour', '8a87b9a8afeead71b0c1e3a704a6f27a.jpg', '2019-05-14 13:44:29', '2019-05-14 13:44:29', 0),
(57, 'Consinco', 'd3cb0f8185b1816c633b8c7d72cabdf5.gif', '2019-05-14 13:44:51', '2019-05-14 13:44:51', 0),
(58, 'Databoff', 'eb66dce3a9024db5cac599005bea4cc0.png', '2019-05-14 13:45:08', '2019-05-14 13:45:08', 0),
(59, 'Ecocentauro', '180a001baa6f74eecbbe471b58b35ebe.png', '2019-05-14 13:45:35', '2019-05-14 13:45:35', 0),
(60, 'Softwar', '39e8594f4fb979fd66dbedc817fdf378.gif', '2019-05-14 13:46:25', '2019-05-14 13:46:25', 0),
(61, 'Lumi', '9fc29f51e07e9dfe5f758d7e66f2b36b.png', '2019-05-14 13:46:52', '2019-05-14 13:46:52', 0),
(62, 'Angulo Sistemas', '6c15084b185a7fc6fdad4ca3c13f65dc.png', '2019-05-14 13:47:15', '2019-05-14 13:47:15', 0),
(63, 'SG Sistemas', '39908e82c2f30de5abfb7aad36fe386f.png', '2019-05-14 13:47:29', '2019-05-14 13:47:29', 0),
(64, 'Software Express', '6616fec1d0a301e3600e27a2e3d9589d.jpg', '2019-05-14 13:54:22', '2019-05-14 13:54:22', 0),
(65, 'RP Info', 'efd5af0eb8f1960cfa3c808c0ffee224.png', '2019-05-14 13:59:09', '2019-05-14 13:59:09', 0),
(66, 'Casa Fiesta', '29e11f3e1cd792a6fa1b3ab915af223c.png', '2019-05-30 10:59:28', '2019-05-30 10:59:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `titulo`, `texto`, `cadastrado_em`, `alterado_em`, `status`) VALUES
(1, 'Teste', 'undefined', '2019-04-11 15:21:30', '2019-04-11 15:21:30', 0),
(2, 'Veja aqui os pontos de coleta do projeto TAMPETS', 'teste de texto', '2019-04-11 15:24:45', '2019-04-11 15:24:45', 0),
(3, 'Veja aqui os pontos de coleta do projeto TAMPETS', 'vsdg ggg', '2019-04-11 15:35:09', '2019-04-11 15:35:09', 0),
(4, 'Teste1', 'dfbd hggjrjrt', '2019-04-11 15:35:42', '2019-04-11 15:35:42', 0),
(5, 'fsdbdebn', 'reherhrehre', '2019-04-11 15:37:12', '2019-04-11 15:37:12', 0),
(6, 'Veja aqui os pontos de coleta do projeto TAMPETS', 'oi', '2019-04-11 15:39:10', '2019-04-11 15:39:10', 0),
(7, 'Veja aqui os pontos de coleta do projeto TAMPETS1', '<p>Veja aqui os pontos de coleta do projeto <strong>TAMPETS<em>1</em></strong></p>', '2019-04-11 15:41:36', '2019-04-11 15:54:00', 0),
(8, 'Moedas de um jeito mais simples', '<p><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Culturalmente no Brasil, devido a momentos hist&oacute;ricos de crise econ&ocirc;mica, existe o que chamamos de entesouramento, que &eacute; o h&aacute;bito de guardar moedas pela popula&ccedil;&atilde;o, fato este que gera um enorme problema para a economia nacional, pois n&atilde;o h&aacute; mais moedas em circula&ccedil;&atilde;o.</span></p>\n<p><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">S&atilde;o aproximadamente 35% das moedas emitidas que deixaram de circular e esse n&uacute;mero aumenta todos os anos, prejudicando principalmente varejista de todos os segmentos que ficam sem troco para seus clientes.</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Com isso, a Troco Simples tem como prop&oacute;sito simplificar de forma criativa as rela&ccedil;&otilde;es financeiras que impactam o nosso dia a dia.</span></p>', '2019-05-04 13:04:53', '2019-05-04 13:04:53', 1),
(9, 'Fazemos a diferença', '<p><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">N&oacute;s da Troco Simples, temos um prop&oacute;sito que vai al&eacute;m de proporcionar uma solu&ccedil;&atilde;o para a falta de moedas no com&eacute;rcio.</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Somos uma empresa 100% digital que se preocupa com a sociedade e por isso, usa os maiores benef&iacute;cios da tecnologia para beneficiar&nbsp;</span><span style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; font-weight: bold; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">consumidores</span><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">&nbsp;e&nbsp;</span><span style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; font-weight: bold; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">neg&oacute;cios</span><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">&nbsp;a usarem o dinheiro de um jeito muito mais inteligente e com isso reduzir custos e perdas.</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Para superar os desafios desse grande sonho, temos como investidores e apoiadores: Curitiba Angels, Darwin Starters que tem como corpora&ccedil;&otilde;es a B3, Transunion, Neoway, RTM e CNSEG, e a Brinks atrav&eacute;s do seu programa de Acelera&ccedil;&atilde;o Brinks UP.</span></p>', '2019-05-04 13:05:43', '2019-05-04 13:05:43', 1),
(10, 'Para cumprirmos o nosso propósito', '<p><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Focamos no que &eacute; importante;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Somos ponte para criar um sistema financeiro mais eficiente;&nbsp;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Criamos solu&ccedil;&otilde;es de infraestrutura financeiras transformando custos em oportunidades;&nbsp;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Melhoramos o relacionamento financeiro entre pessoas e empresas;&nbsp;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Proporcionamos experi&ecirc;ncias inovadoras com solu&ccedil;&otilde;es criativas.</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Quer fazer parte dessa mudan&ccedil;a? Ent&atilde;o&nbsp;</span><a style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; cursor: pointer; color: #535353; font-weight: bold; font-family: Lato; font-size: 18px; background-color: #ffffff;\" href=\"../index.php?pg=sobre\">Fale Conosco</a><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">&nbsp;e veja como come&ccedil;ar.</span></p>', '2019-05-04 13:06:31', '2019-05-04 13:06:31', 1),
(11, 'No que acreditamos', '<p><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Nossa Miss&atilde;o &eacute; simplificar as transa&ccedil;&otilde;es financeiras que envolvam dinheiro em esp&eacute;cie atrav&eacute;s de uma rede que conecta consumidores, varejistas e domic&iacute;lios (desde bancos e wallets, &agrave; servi&ccedil;os de assinatura, transporte e telefonia), para a devolu&ccedil;&atilde;o em troco digital para compras com dinheiro.</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Sonhamos em ser a maior rede de solu&ccedil;&otilde;es para transa&ccedil;&otilde;es financeiras. E com isso, continuar a simplificar o dia a dia da sociedade que geram impactos reais a economia.</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">Nossos valores:</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Retid&atilde;o; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Iniciativa;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Comprometimento; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Empatia;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Pensamento Cr&iacute;tico; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Generosidade;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Resili&ecirc;ncia; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Positividade;</span><br style=\"-webkit-font-smoothing: antialiased; margin: 0px; padding: 0px; clear: both; color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\" /><span style=\"color: #535353; font-family: Lato; font-size: 18px; background-color: #ffffff;\">- Coopera&ccedil;&atilde;o; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ownership.</span></p>', '2019-05-04 13:07:20', '2019-05-04 13:07:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notas_imprensa`
--

CREATE TABLE `notas_imprensa` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `resumo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='	';

-- --------------------------------------------------------

--
-- Table structure for table `revendedores`
--

CREATE TABLE `revendedores` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade_uf` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fone` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `revendedores_candidatos`
--

CREATE TABLE `revendedores_candidatos` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sobrenome` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fone` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci,
  `curriculo` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sistemas`
--

CREATE TABLE `sistemas` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` tinyint(4) DEFAULT NULL COMMENT '1=> TEF; 2=> Frente de Caixa',
  `cadastrado_em` datetime DEFAULT NULL,
  `alterado_em` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categorias`
--
ALTER TABLE `blog_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_post_fotos`
--
ALTER TABLE `blog_post_fotos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_blog_post_fotos_blog_post_idx` (`blog_post_id`);

--
-- Indexes for table `campanhas`
--
ALTER TABLE `campanhas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notas_imprensa`
--
ALTER TABLE `notas_imprensa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revendedores`
--
ALTER TABLE `revendedores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revendedores_candidatos`
--
ALTER TABLE `revendedores_candidatos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sistemas`
--
ALTER TABLE `sistemas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `blog_categorias`
--
ALTER TABLE `blog_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_post_fotos`
--
ALTER TABLE `blog_post_fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campanhas`
--
ALTER TABLE `campanhas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `notas_imprensa`
--
ALTER TABLE `notas_imprensa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revendedores`
--
ALTER TABLE `revendedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revendedores_candidatos`
--
ALTER TABLE `revendedores_candidatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sistemas`
--
ALTER TABLE `sistemas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_post_fotos`
--
ALTER TABLE `blog_post_fotos`
  ADD CONSTRAINT `fk_blog_post_fotos_blog_post` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
