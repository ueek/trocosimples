<section class="banner-internas"></section>

<section class="sep-banner"></section>

<section id="entidade">
	<div class="conteudo">

		<h1 class="">A segunda casa do seu troco digital <br>pode fazer a diferença na vida de muita gente</h1>

		<img class="" src="img/mao-rs.png" alt="TrocoSimples">

		<h2 class="">A Troco Simples em parceria com o Polen apoia causas sociais e você também pode ajudar.</h2>

		<p class="">
			Ao receber o Troco Digital, a segunda casa depois da vírgula do seu troco e doado automaticamente. <br>
			A Troco Simples respeita o seu poder de escolha, e com isso se caso você não queira doar, <strong>dentro do app é possível desabilitar a doação automática.</strong> Os seus centavinhos somam-se à centenas de outros centavinhos dos usuários da Troco Simples e podem juntos colaborar numa grande causa.
		</p>

		<img class="" src="img/duvida.png" alt="TrocoSimples">

		<h2 class="">Como isso é feito?</h2>

		<p class="">
			A Troco Simples em parceria com o Polen, especialista em alavancar a habilidade de impacto Social do varejo, trabalham juntas nesta causa para revolucionar o processo de consumo e incluir um impacto social positivo em cada compra. Hoje o Polen conta com um amplo portfólio de ONG’s que passam por processos cuidadosos de curadoria, e de centavo em centavo já destinaram mais de R$200.000,00 para causas sociais. 
			A Troco Simples também acredita no poder do impacto social pelo consumo, por isso, o Polen será o agente responsável por multiplicar o potencial dos seus centavos, garantindo que parte de seu troco seja transformado em recursos livres para mais de 300 instituições.
		</p>

		<div id="itens-entidade" class="display-desktop">
			<img src="img/entidade.png" alt="TrocoSimples">
		</div>
		<div id="itens-entidade" class="display-mobile">
			<img src="img/entidade-mobile.png" alt="TrocoSimples">
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->