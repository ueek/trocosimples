<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-paraempresas.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section class="formularios" id="formulario">
	<div class="conteudo">

		<div class="formulario-sucesso" style="display: block;">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Obrigado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->