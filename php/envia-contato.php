<?php	

	//Configura��es
	$destinatario    = "contato@trocosimples.com.br";
	//$copia   		 = "petilin@urbexp.com.br";
	//$emailRemetente = "rafael.branco@ueek.com.br";
	//$CopiaRemetente = "suporte@ueek.com.br";



	//Dados do formulario
	$nomeRemetente      	= $_POST['nome'];
	$emailRemetente     	= $_POST['email'];
	/*$mensagemRemetente  	= $_POST['mensagem'];*/


	$dataHora           = date("d/m/Y")." as ".date("H:i:s");
    
    $msgHora = utf8_encode("Hor�rio da mensagem: {$dataHora}");

	$dadosUsuarios = utf8_encode("Dados do Usu�rio:");

	$txt = utf8_encode("Ol�, voc� recebeu uma nova mensagem de contato enviada atrav�s do site www.trocosimples.com.br");



	$titulo = utf8_encode("Mensagem de contato - Troco Simples");

	//Montando o cabe�alho da mensagem
  	/*$headers = implode ("\n", array("From: $emailRemetente", 
  									"Reply-To: $emailRemetente", 
  									"Subject: $titulo",
  									"Return-Path: $emailRemetente",
  									"MIME-Version: 1.0",
  									"X-Priority: 3",
  									"Content-Type: text/html; charset=UTF-8"));*/
    

    //Montando o cabe�alho da mensagem
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: $emailRemetente\r\n"; // remetente
    //$headers .= "Cc: $copia\r\n"; // C�pia
    $headers .= "Return-Path: $emailRemetente\r\n"; // return-path


    //Valida��es
	if(empty($nomeRemetente) or empty($emailRemetente)){
		$retorno = array("status" => 0, "msg" => "Todos os campos em destaque devem ser preenchidos!");
		echo json_encode($retorno);
		exit;
	}


	//Texto do e-mail
	$mensagem = "<!DOCTYPE html>
	<html lang='pt-br'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<meta charset='utf-8'>

		<title>$titulo</title>		
	</head>

	<body>
		$txt <br><br>

		<strong>{$dadosUsuarios}</strong><br>
		Nome: {$nomeRemetente}<br>
		E-mail: {$emailRemetente}<br><br>

		{$msgHora}
	</body>

	</html>"; // Corpo da mensagem em formato HTML


	// Enviando a mensagem
	if(mail($destinatario, $titulo, $mensagem, $headers)){
		$retorno = array("status" => 1, "msg" => "Mensagem de contato enviada com sucesso! Em breve entraremos em contato.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Falha ao enviar mensagem de contato, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>