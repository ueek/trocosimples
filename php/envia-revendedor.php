<?php	

	$destinatario    = "contato@trocosimples.com.br";


	//Dados do formulario
	$nomeRemetente      	= $_POST['nome'];
	$emailRemetente     	= $_POST['email'];


	$dataHora           = date("d/m/Y")." as ".date("H:i:s");
    $msgHora = "Horário da mensagem: {$dataHora}";

	$dadosUsuarios = "Dados do Usuário:";

	$txt = "Olá, você recebeu uma nova mensagem de contato enviada através do site www.trocosimples.com.br";


	$titulo = "Mensagem de contato - Troco Simples";
	
    
    //Montando o cabeçalho da mensagem
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: $emailRemetente\r\n"; // remetente
    $headers .= "Return-Path: $emailRemetente\r\n"; // return-path
	
    
    //Texto do e-mail
	$mensagem = "<!DOCTYPE html>
	<html lang='pt-br'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<meta charset='utf-8'>

		<title>$titulo</title>		
	</head>

	<body>
		$txt <br><br>

		<strong>{$dadosUsuarios}</strong><br>
		Nome: {$nomeRemetente}<br>
		E-mail: {$emailRemetente}<br><br>

		{$msgHora}
	</body>

	</html>"; // Corpo da mensagem em formato HTML

	// Enviando a mensagem
	if(mail($destinatario, $titulo, $mensagem, $headers)){
		$retorno = array("status" => 1, "msg" => "Mensagem de contato enviada com sucesso! Em breve entraremos em contato.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Falha ao enviar mensagem de contato, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>