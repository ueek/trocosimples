<?php
include_once 'base-model.php';
class BlogPostFoto extends BaseModel
{
    // object properties
    private $blog_post_id;
    private $imagem;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("blog_post_fotos");
	}


    /**
     * @return mixed
     */
    public function getBlogPostId()
    {
        return $this->blog_post_id;
    }

    /**
     * @param mixed $blog_post_id
     *
     * @return self
     */
    public function setBlogPostId($blog_post_id)
    {
        $this->blog_post_id = $blog_post_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     *
     * @return self
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;

        return $this;
    }

    function readAllByBlogPostId($blog_post_id){
        $query= "SELECT * 
                FROM ".$this->getTableName()."
                 WHERE blog_post_id= {$blog_post_id} AND status=1";

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        // $arr = $stmt->errorInfo();

        // error_log($query, 0);

        return $stmt;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    blog_post_id=:blog_post_id, 
                    imagem=:imagem, 
                    cadastrado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->blog_post_id=htmlspecialchars(strip_tags($this->blog_post_id));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
     
        // bind values
        $stmt->bindParam(":blog_post_id", $this->blog_post_id);
        $stmt->bindParam(":imagem", $this->imagem);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    blog_post_id=:blog_post_id, 
                    imagem=:imagem
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->blog_post_id=htmlspecialchars(strip_tags($this->blog_post_id));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":blog_post_id", $this->blog_post_id);
        $stmt->bindParam(":imagem", $this->imagem);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}