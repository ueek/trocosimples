<?php
include_once 'base-model.php';
class RevendedorCandidato extends BaseModel
{
    // object properties
    private $nome;
    private $sobrenome;
    private $cidade;
    private $fone;
    private $email;
    private $mensagem;
    private $curriculo;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("revendedores_candidatos");
	}

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    /**
     * @param mixed $sobrenome
     *
     * @return self
     */
    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     *
     * @return self
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFone()
    {
        return $this->fone;
    }

    /**
     * @param mixed $fone
     *
     * @return self
     */
    public function setFone($fone)
    {
        $this->fone = $fone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMensagem()
    {
        return $this->mensagem;
    }

    /**
     * @param mixed $mensagem
     *
     * @return self
     */
    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurriculo()
    {
        return $this->curriculo;
    }

    /**
     * @param mixed $curriculo
     *
     * @return self
     */
    public function setCurriculo($curriculo)
    {
        $this->curriculo = $curriculo;

        return $this;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    sobrenome=:sobrenome, 
                    cidade=:cidade, 
                    fone=:fone, 
                    email=:email, 
                    mensagem=:mensagem, 
                    curriculo=:curriculo, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->sobrenome=htmlspecialchars(strip_tags($this->sobrenome));
        $this->cidade=htmlspecialchars(strip_tags($this->cidade));
        $this->fone=htmlspecialchars(strip_tags($this->fone));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->mensagem=htmlspecialchars(strip_tags($this->mensagem));
        $this->curriculo=htmlspecialchars(strip_tags($this->curriculo));
     
        // bind values
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":sobrenome", $this->sobrenome);
        $stmt->bindParam(":cidade", $this->cidade);
        $stmt->bindParam(":fone", $this->fone);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":mensagem", $this->mensagem);
        $stmt->bindParam(":curriculo", $this->curriculo);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->nome=htmlspecialchars(strip_tags($this->nome));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":nome", $this->nome);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}