<?php
include_once 'base-model.php';
class Revendedor extends BaseModel
{
    // object properties
    private $nome;
    private $cidade_uf;
    private $fone;
    private $email;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("revendedores");
	}


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCidadeUf()
    {
        return $this->cidade_uf;
    }

    /**
     * @param mixed $cidade_uf
     *
     * @return self
     */
    public function setCidadeUf($cidade_uf)
    {
        $this->cidade_uf = $cidade_uf;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFone()
    {
        return $this->fone;
    }

    /**
     * @param mixed $fone
     *
     * @return self
     */
    public function setFone($fone)
    {
        $this->fone = $fone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    cidade_uf=:cidade_uf, 
                    fone=:fone, 
                    email=:email, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->cidade_uf=htmlspecialchars(strip_tags($this->cidade_uf));
        $this->fone=htmlspecialchars(strip_tags($this->fone));
        $this->email=htmlspecialchars(strip_tags($this->email));
     
        // bind values
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":cidade_uf", $this->cidade_uf);
        $stmt->bindParam(":fone", $this->fone);
        $stmt->bindParam(":email", $this->email);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    cidade_uf=:cidade_uf, 
                    fone=:fone, 
                    email=:email, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->cidade_uf=htmlspecialchars(strip_tags($this->cidade_uf));
        $this->fone=htmlspecialchars(strip_tags($this->fone));
        $this->email=htmlspecialchars(strip_tags($this->email));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":cidade_uf", $this->cidade_uf);
        $stmt->bindParam(":fone", $this->fone);
        $stmt->bindParam(":email", $this->email);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}