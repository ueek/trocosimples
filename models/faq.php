<?php
include_once 'base-model.php';
class Faq extends BaseModel
{
    // object properties
    private $titulo;
    private $texto;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("faqs");
	}


    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * @param mixed $texto
     *
     * @return self
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    texto=:texto, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
     
        // bind values
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":texto", $this->texto);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    texto=:texto, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":texto", $this->texto);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}