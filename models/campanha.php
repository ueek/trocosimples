<?php
include_once 'base-model.php';
class Campanha extends BaseModel
{
    // object properties
    private $titulo;
    private $resumo;
    private $url;
    private $imagem;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("campanhas");
	}



    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResumo()
    {
        return $this->resumo;
    }

    /**
     * @param mixed $resumo
     *
     * @return self
     */
    public function setResumo($resumo)
    {
        $this->resumo = $resumo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     *
     * @return self
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;

        return $this;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    imagem=:imagem, 
                    resumo=:resumo, 
                    url=:url, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
        $this->resumo=htmlspecialchars(strip_tags($this->resumo));
        $this->url=htmlspecialchars(strip_tags($this->url));
     
        // bind values
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":imagem", $this->imagem);
        $stmt->bindParam(":resumo", $this->resumo);
        $stmt->bindParam(":url", $this->url);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    imagem=:imagem, 
                    resumo=:resumo, 
                    url=:url, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
        $this->resumo=htmlspecialchars(strip_tags($this->resumo));
        $this->url=htmlspecialchars(strip_tags($this->url));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":imagem", $this->imagem);
        $stmt->bindParam(":resumo", $this->resumo);
        $stmt->bindParam(":url", $this->url);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }


}