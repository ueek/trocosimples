<?php
include_once 'base-model.php';
class Cliente extends BaseModel
{
    // object properties
    private $nome;
    private $imagem;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("clientes");
	}


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     *
     * @return self
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;

        return $this;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    imagem=:imagem, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
     
        // bind values
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":imagem", $this->imagem);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    imagem=:imagem, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":imagem", $this->imagem);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}