<?php
include_once 'base-model.php';
class BlogPost extends BaseModel
{
    // object properties
    private $categoria_ids;
    private $titulo;
    private $texto;
    private $url;


	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("blog_posts");
	}

    /**
     * @return mixed
     */
    public function getCategoriaIds()
    {
        return $this->categoria_ids;
    }

    /**
     * @param mixed $categoria_ids
     *
     * @return self
     */
    public function setCategoriaIds($categoria_ids)
    {
        $this->categoria_ids = $categoria_ids;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * @param mixed $texto
     *
     * @return self
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    function readPostsWithMainPicture($limit = "", $filter = ""){
        $query = "SELECT post.*,
                (
                    SELECT fotos.imagem
                    FROM blog_post_fotos fotos
                    WHERE fotos.blog_post_id = post.id
                    LIMIT 1
                ) AS imagem 
                FROM blog_posts post
                WHERE post.status = 1 ".$filter." 
                ORDER BY post.id DESC ".$limit;

            error_log($query);

            // Executar a query e retornar os resultados
            $stmt = $this->getConn()->prepare( $query );
            $stmt->execute();

            $arr = $stmt->errorInfo();

            return $stmt;
    }

    function readByUrl($url){
        $query = "SELECT post.*,
                (
                    SELECT fotos.imagem
                    FROM blog_post_fotos fotos
                    WHERE fotos.blog_post_id = post.id
                    LIMIT 1
                ) AS imagem 
                FROM blog_posts post
                WHERE post.status = 1 AND post.url = '{$url}'";

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        return $stmt;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    texto=:texto, 
                    categoria_ids=:categoria_ids, 
                    url=:url, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
        //$this->texto=htmlspecialchars(strip_tags($this->texto));
        $this->categoria_ids=htmlspecialchars(strip_tags($this->categoria_ids));
        $this->url=htmlspecialchars(strip_tags($this->url));
     
        // bind values
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":texto", $this->texto);
        $stmt->bindParam(":categoria_ids", $this->categoria_ids);
        $stmt->bindParam(":url", $this->url);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    texto=:texto, 
                    categoria_ids=:categoria_ids, 
                    url=:url, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
        //$this->texto=htmlspecialchars(strip_tags($this->texto));
        $this->categoria_ids=htmlspecialchars(strip_tags($this->categoria_ids));
        $this->url=htmlspecialchars(strip_tags($this->url));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":texto", $this->texto);
        $stmt->bindParam(":categoria_ids", $this->categoria_ids);
        $stmt->bindParam(":url", $this->url);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}