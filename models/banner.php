<?php
include_once 'base-model.php';
class Banner extends BaseModel
{
    // object properties
    private $titulo;
    private $subtitulo;
    private $texto;
    private $url;
    private $imagem;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("banners");
	}

   function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    subtitulo=:subtitulo, 
                    texto=:texto, 
                    url=:url, 
                    imagem=:imagem, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
        $this->subtitulo=htmlspecialchars(strip_tags($this->subtitulo));
        $this->texto=htmlspecialchars(strip_tags($this->texto));
        $this->url=htmlspecialchars(strip_tags($this->url));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
     
        // bind values
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":subtitulo", $this->subtitulo);
        $stmt->bindParam(":texto", $this->texto);
        $stmt->bindParam(":url", $this->url);
        $stmt->bindParam(":imagem", $this->imagem);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    titulo=:titulo, 
                    subtitulo=:subtitulo, 
                    texto=:texto, 
                    url=:url, 
                    imagem=:imagem, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->titulo=htmlspecialchars(strip_tags($this->titulo));
        $this->subtitulo=htmlspecialchars(strip_tags($this->subtitulo));
        $this->texto=htmlspecialchars(strip_tags($this->texto));
        $this->url=htmlspecialchars(strip_tags($this->url));
        $this->imagem=htmlspecialchars(strip_tags($this->imagem));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":titulo", $this->titulo);
        $stmt->bindParam(":subtitulo", $this->subtitulo);
        $stmt->bindParam(":texto", $this->texto);
        $stmt->bindParam(":url", $this->url);
        $stmt->bindParam(":imagem", $this->imagem);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubtitulo()
    {
        return $this->subtitulo;
    }

    /**
     * @param mixed $subtitulo
     *
     * @return self
     */
    public function setSubtitulo($subtitulo)
    {
        $this->subtitulo = $subtitulo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * @param mixed $texto
     *
     * @return self
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     *
     * @return self
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;

        return $this;
    }
}