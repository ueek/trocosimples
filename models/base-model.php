<?php
class BaseModel
{
    // Conexão de BD
    private $conn;
    private $table_name = "";

    // object properties
    private $id;

    private $cadastrado_em;
    private $alterado_em;
    private $status;
    

     /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * @param mixed $table_name
     *
     * @return self
     */
    public function setTableName($table_name)
    {
        $this->table_name = $table_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCadastradoEm()
    {
        return $this->cadastrado_em;
    }

    /**
     * @param mixed $cadastrado_em
     *
     * @return self
     */
    public function setCadastradoEm($cadastrado_em)
    {
        $this->cadastrado_em = $cadastrado_em;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlteradoEm()
    {
        return $this->alterado_em;
    }

    /**
     * @param mixed $alterado_em
     *
     * @return self
     */
    public function setAlteradoEm($alterado_em)
    {
        $this->alterado_em = $alterado_em;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
    
    function read($orderBy = "Id", $limit = "", $filter = ""){
        try{
            $query= "SELECT * 
                    FROM ".$this->table_name."
                     WHERE status= 1 ".$filter.
                    " ORDER BY ".$orderBy." ".$limit;

            if ($filter != "") {
                error_log($query, 0);
            }
            
            // Executar a query e retornar os resultados
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();

            //$arr = $stmt->errorInfo();

            return $stmt;
        }catch(Exception $ex){
        }
    }

    function readById($id){
        try{
            $query= "SELECT * 
                    FROM ".$this->table_name."
                     WHERE id= {$id}";

            // Executar a query e retornar os resultados
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();

            $arr = $stmt->errorInfo();

            return $stmt;
        }catch(Exception $ex){
        }
    }

    function delete($id){
       $query= "UPDATE ".$this->table_name."
                 SET status=0 
                 WHERE id= {$id}";

         error_log($query, 0);
         
        // Executar a query e retornar os resultados
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();

        $arr = $stmt->errorInfo();

        return $stmt;
    }

 

   
}