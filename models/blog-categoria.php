<?php
include_once 'base-model.php';
class BlogCategoria extends BaseModel
{
    // object properties
    private $nome;
    private $url;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("blog_categorias");
	}

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    url=:url, 
                    cadastrado_em=NOW(),
                    alterado_em=NOW(),
                    status=1";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->url=htmlspecialchars(strip_tags($this->url));
     
        // bind values
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":url", $this->url);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    nome=:nome, 
                    url=:url, 
                    alterado_em=NOW() 
                WHERE id=:id";

        // prepare query
        $stmt = $this->getConn()->prepare($query);
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->nome=htmlspecialchars(strip_tags($this->nome));
        $this->url=htmlspecialchars(strip_tags($this->url));
     
        // bind values
        $id = $this->getId();
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":nome", $this->nome);
        $stmt->bindParam(":url", $this->url);
     
        // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }


}