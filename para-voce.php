<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-paravoce.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="pg-para-voce">
	<div class="conteudo">
		<h1>O “cofrinho” moderno e simples de usar.</h1>

		<h2>A Troco Simples é o melhor jeito de carregar e guardar suas moedas. </h2>

		<div id="box-paravoce" class="">
			
			<div id="itens-paravoce">				
				<div class="item-paravoce" id="left">
					<img src="img/sem-download.png" alt="TrocoSimples">

					<h3>Sem download</h3>
				</div>
				<div class="item-paravoce" id="center">
					<img src="img/sem-cadastro.png" alt="TrocoSimples">

					<h3>Sem cadastro</h3>
				</div>
				<div class="item-paravoce" id="right">
					<img src="img/sem-custo.png" alt="TrocoSimples">

					<h3>Sem custo</h3>
				</div>

				<br>
			</div>

			<span>Receba troco digital direto no seu CPF e pare de perder dinheiro por aí.</span>
		</div>

		<p class="">
			Você recebe <strong>troco digital</strong> direto no seu CPF em toda a rede de parceiros Troco Simples. Você só precisará do app para escolher para onde quer destinar seu <strong>troco digital</strong>, e pronto!
		</p>
	</div>
</section>

<section id="como-funciona">
	<div id="left-comofunciona"></div>

	<div id="right-comofunciona">
		<div id="conteudo-comofunciona">
			<h1>COMO FUNCIONA?</h1>

			<h2>Troco em bala? Aqui não!</h2>

			<h3>Com Troco Simples o seu troco é digital!</h3>

			<ul class="">
				<li>Aceite troco digital nas suas compras em dinheiro na rede de parceiros Troco Simples;</li><br>
				<li>Digite ou informe o seu CPF* para receber o troco digital;</li><br>
				<li>Pronto! Você já está acumulando troco digital.</li>
			</ul>

			<p class="">
				*Você não precisa estar cadastrado ou usar o aplicativo para começar a acumular seu troco digital. Pelo app, seu saldo não expira e você pode baixar quando quiser para escolher a melhor forma de usá-lo.
			</p>
		</div>
	</div>
	<br>
</section>

<section id="varias-formas">
	<div class="conteudo">
		<h1>São várias formas de usar o seu troco digital.</h1>

		<div id="itens-variasformas" class="">
			<div class="item-variasformas left">
				<img src="img/transferencia-automatica.png" alt="TrocoSimples">

				<h2>Transfira automaticamente <br>para a sua conta bancária</h2>
			</div>
			<div class="item-variasformas center">
				<img src="img/rendimentos-app.png" alt="TrocoSimples">

				<h2>Mantenha o seu troco <br>rendendo até 6% a.a. no app</h2>
			</div>
			<div class="item-variasformas right">
				<img src="img/pagar-conta.png" alt="TrocoSimples">

				<h2>Pague a sua conta <br>na rede de parceiros</h2>
			</div>
			<br>
			<div class="item-variasformas left">
				<img src="img/inserir-creditos.png" alt="TrocoSimples">

				<h2>Recarregue seu <br>celular pré-pago</h2>
			</div>
			<div class="item-variasformas center">
				<img src="img/enviar-dinheiro.png" alt="TrocoSimples">

				<h2>Enviar dinheiro <br>para um amigo</h2>
			</div>
			<div class="item-variasformas right">
				<img src="img/doe.png" alt="TrocoSimples">

				<h2>Doe a segunda casa depois da vírgula <br>para Instituições de Caridade</h2>
			</div>
			<br>
		</div>
	</div>
</section>

<section class="baixe-agora">
	<div class="conteudo">
		<h1>Se quiser, baixe agora para ver como é simples de usar:</h1>

		<div class="itens-baixeagora ">
			<a target="_blank" href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8&utm_source=site&utm_medium=botao&utm_content=ios">
				<img src="img/ios.png" alt="TrocoSimples">
			</a>
			<a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor&utm_source=site&utm_medium=botao&utm_content=android">
				<img src="img/android.png" alt="TrocoSimples">
			</a>
			<br>
		</div>
	</div>
</section>

<section id="vantagens-receber">
	<div class="conteudo">
		<h1>Vantagens de receber o seu troco digital</h1>

		<p id="txt-vantagens">
			Quem escolhe receber o troco digital tem muitas vantagens, pois além de você não ficar sem o troco que é seu direito, muitos dos parceiros Troco Simples oferecem troco em dobro para quem aceita o troco digital.
		</p>

		<div id="itens-vantagens">
			
			<div id="center" class="">
				<h2>Troco em dobro</h2>

				<h3>Fique ligado!</h3>

				<img id="vantagem-mobile" src="img/vantagens-left.png" alt="TrocoSimples">

				<p id="desc-vantagens">
					Muitos parceiros dobram o troco entregue.<br>
					Por exemplo, seu troco de <strong>R$1,50</strong> vira <strong>R$3,00</strong>.<br>
					Além disso, muitos brindes e descontos.
				</p>
			</div>

			<img id="left" class="" src="img/vantagens-left.png" alt="TrocoSimples">

			<img id="right" class="" src="img/vantagens-right.png" alt="TrocoSimples">
			<br>

		</div>

		<img id="flexa-vantagens" class="" src="img/flexa-vantagens.png" alt="TrocoSimples">

<!-- 		<a href="#">
			<div class="btn-estabelecimentos-participantes soft-hover">Estabelecimentos Participantes</div>
		</a> -->

	</div>
</section>

<section id="motivos-receber">

	<div id="motivos-receber-left"></div>

	<div id="motivos-receber-right">		
		<div id="motivos-receber-info">
			
			<h1>Motivos para receber o seu troco digital:</h1>

			<p>
				Com o troco digital você também colabora para solucionar o problema de escassez de moedas no comércios, continua acumulando no seu cofrinho e ajuda a reduzir gastos para a economia.
				O intuito dá Troco Simples vai muito além de ajudar o seu bolso: nós trazemos resultados reais para a realidade econômica do Brasil.
	 			Faça parte dessa rede, <strong>BAIXE AGORA</strong> o aplicativo Troco Simples!
			</p>

			<div id="itens-motivosreceber" class="">
				<a target="_blank" href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8&utm_source=site&utm_medium=botao&utm_content=ios">
					<img src="img/ios.png" alt="TrocoSimples">
				</a>
				<a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor&utm_source=site&utm_medium=botao&utm_content=android">
					<img src="img/android.png" alt="TrocoSimples">
				</a>
				<br>
			</div>

		</div>
	</div>
	<br>
	
</section>

<section id="quer-outro">
	<div class="conteudo">
		<h1>Quer outro grande motivo para aceitar seu troco digital?</h1>

		<div class="display-desktop ">
			<img src="img/quer-outro.png" alt="TrocoSimples">
		</div>

		<div class="display-mobile ">
			<img src="img/quer-outro-mobile.png" alt="TrocoSimples">
		</div>

		<p class="">
			Se você optar por receber o troco digital, <span>a segunda casa depois da vírgula</span> é doada automaticamente para instituições de caridade. É um jeito muito simples de ajudar quem mais precisa.
 			Os seus centavinhos somam-se à centenas de outros centavinhos dos usuários da Troco Simples e podem juntos colaborar numa grande causa.
		</p>
	</div>
</section>

<section class="clientes-parceiros">
	<div class="conteudo">
		<h1>Clientes & Parceiros</h1>

		<p class="display-desktop">
			Vamos juntos mudar a forma como os brasileiros se relacionam com o troco. <br>Faça parte da rede Troco Simples e peça seu troco digital.
		</p>

		<span class="display-mobile">Conheça alguns dos clientes parceiros da Troco Simples</span>

		<div class="itens-clientesparceiros regular5">
			<?php 

				while ($row = $myClientes->fetch(PDO::FETCH_ASSOC)){
					extract($row);

					$urlImagem = "gestor/uploads/clientes/".$imagem; 

					echo"
						<div>
					      	<div class='item-clientesparceiros' style='background: url($urlImagem); background-position: center; background-repeat: no-repeat; background-size: contain;'>
					      	</div>
					    </div>
					";
					
				}

			?>

			<br>
		</div>
	</div>
</section>

<section id="ainda-duvida">
	<div class="conteudo">

		<img id="left-aindaduvida" src="img/left-aindaduvida.png" alt="TrocoSimples">

		<div id="right-aindaduvida">
			
			<h1>Ainda na dúvida?</h1>

			<span>Então vamos te contar algumas coisas que você não sabe:</span>

			<p class="">
				Segundo o Banco Central, cerca de 35% das moedas emitidas no país estão fora de circulação, principalmente devido aos “cofrinhos”. Isso representa 8,7 bilhões de  moedas, número que vem aumentando todos os anos.

				Além disso, a produção de moedas tem um valor maior para os cofres do país do que seu valor no mercado. Por exemplo:
	 			<strong>Para produzirmos uma moeda de R$ 0,05, os custos de produção são de R$ 0,30.</strong><br>

				Diante desse cenário é óbvio que a conta não fecha. Então vamos juntos mudar para o troco digital?
			</p>

			<div id="itens-aindaduvida" class="">
				<a target="_blank" href="https://itunes.apple.com/br/app/ts-troco-simples/id1441671513?mt=8&utm_source=site&utm_medium=botao&utm_content=ios">
					<img src="img/ios.png" alt="TrocoSimples">
				</a>
				<a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.trocosimples.consumidor&utm_source=site&utm_medium=botao&utm_content=android">
					<img src="img/android.png" alt="TrocoSimples">
				</a>
				<br>
			</div>

		</div>
		<br>
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->