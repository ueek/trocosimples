<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-paraempresassaibamais.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="para-empresas-saiba-mais">
	<div class="conteudo">
		<h1>Saiba Mais</h1>

		<h2>Empresas</h2>

		<p>
			A Troco Simples entende a realidade dos varejistas em relação a falta de moedas e pequenas notas para troco. Por isso, foi criada com o propósito  de simplificar as transações financeiras do varejo que envolvem dinheiro em espécie, transformando valores em saldo no CPF do cliente com troco digital. 
			<br><br>
			Atualmente, vivemos em um momento de grande escassez de moedas em circulação no mercado. Segundo o Banco Central esse problema acontece devido ao costume das pessoas em criar “cofrinhos”, o que já gerou um prejuízo de aproximadamente 3,7 bilhões de reais (valor de moedas paradas no Brasil), isso representa cerca de 35% das moedas já emitidas no país.

		</p>

		<h2>“Ah, mas por que o Banco Central simplesmente não emite mais moedas e acaba com este problema?”</h2>

		<p>
			Os valores para a produção de moedas não fecham. Por exemplo, para a produção de uma moeda de R$ 0,05, o valor gasto é de R$ 0,30 e com as demais moedas não é diferente. Por isso, o Banco Central reduziu significativamente a produção..<br>
			Com isso, a falta moedas nos comércios e os valores das compras acabam sendo arredondados para não gerar desgaste com clientes.. No entanto , esta atitude gera um grande prejuízo para os empresários, que acabam perdendo também  muito tempo para adquirir   moedas ou outras soluções que reduzam esse impacto nos caixas, além de gerar ainda mais custos com captação e transporte das mesmas.<br><br>

			Mas os problemas não param por aí, pois os comerciantes se vêem obrigados a criar um fundo de caixa para tentar manter seu fluxo e rotatividade, ou seja, ainda mais prejuízo. 

		</p>

		<h2>A Troco Simples chegou para acabar com este problema e com todo esse desperdício de dinheiro. </h2>

		<p>
			De um jeito rápido e fácil, você pode integrar a Troco Simples ao seu sistema PDV e TEF. Já são dezenas de parceiros de frente de caixa homologados e queremos aumentar ainda mais este número. E mesmo que você não trabalhe com nenhum desses sistemas, possuímos  uma API  própria para comunicação direta ou através do módulo Troco Simples dentro dos principais TEF, para que você não perca a oportunidade de mudar a realidade do seu negócio.
			<br><br>
			É  por meio  de um desses meios que a entrega do troco digital é feita. Com   uma recarga pré-paga em dinheiro para a Troco Simples, você gera um saldo digital em sua conta que será transformado em troco para seus clientes. Você fará a transferência do troco digital direto do seu sistema para o CPF do consumidor. Não é genial?
			<br><br>
			A melhor parte é que ele não precisa estar cadastrado ou baixar o aplicativo. Todo o saldo que ele receber nos parceiros dá Troco Simples ficam depositados no CPF dele que só precisará do app para escolher como quer usar o troco digital.
			<br><br>
			Este novo método de entregar e receber o troco é  bom  pra todo mundo: para economia nacional, para nós, para sua empresa e para os seus clientes que também terão muitos benefícios. Você pode <a href="#" target="_blank">ler aqui</a> quais são eles para poder explicar o por que eles podem e devem aceitar receber o troco digital.
		</p>

		<h2>
			Ainda não te convencemos? Então preencha o formulário abaixo e deixe que um de nossos revendedores entre em contato com você para te apresentar a Troco Simples. Nós vamos te explicar sem nenhum compromisso a forma como a Troco Simples pode mudar o seu negócio.
		</h2>

	</div>
</section>

<section class="formularios">
	<div class="conteudo">
		<h1>PARA EMPRESAS</h1>

		<h2>Saia do prejuízo pela falta de moedas</h2>

		<p>
			Rodamos integrados ao seu frente de caixa (PDV) e ou TEF,  para que você entregue troco digital direto no CPF dos seus clientes.
		</p>

		<form class="formulario">
			<div class="col-1">
				
				<label>Nome*</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>Cidade*</label>
				<input class="soft-hover" type="text" name="cidade" placeholder="">

				<label>E-mail*</label>
				<input class="soft-hover" type="text" name="email" placeholder="">

				<label>Segmento*</label>
				<input class="soft-hover" type="text" name="segmento" placeholder="">

			</div>
			<div class="col-2">
				<label>Último Nome*</label>
				<input class="soft-hover" type="text" name="ultimonome" placeholder="">

				<label>Telefone*</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<label>Empresa*</label>
				<input class="soft-hover" type="text" name="empresa" placeholder="">

				<label>Número de clientes*</label>
				<input class="soft-hover" type="text" name="numeroclientes" placeholder="">
			</div>
			<br>

			<input class="soft-hover" type="submit" value="Enviar Formulário">
			
			<span class="">
				Preencha o formulário e descubra como começar a usar o troco digital. O contato é explicativo e totalmente sem compromisso. Campos marcados por um * são obrigatórios.
			</span>
		</form>

		<div class="btn-formularios">Saiba Mais</div>

		<div class="formulario-sucesso">
			<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

			<div>Seu formulário foi submetido com sucesso.</div>
		</div>

		<div class="formulario-erro">
			<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

			<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
		</div>

	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->