<?php
    include_once 'models/faq.php';

	// Instanciar objetos
	$faq = new Faq($db);
	$myFaqs = $faq->read();
?>

<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-sobrenos.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="troco-simples">
	<div class="conteudo">
		<h1>Simplificar é o nosso propósito!</h1>

		<div id="itens-trocosimples" class="">

			<?php 

				while ($row = $myFaqs->fetch(PDO::FETCH_ASSOC)){
					extract($row);


					echo"
						<div class='item-trocosimples'>
							<div class='controla-itens'>
								<h2>$titulo <span>+</span></h2>
							</div>

							<div class='txt-trocosimples'>
								$texto
							</div>
						</div>
					";
					
				}

			?>

		</div>

	</div>
</section>

<section id="veja-como">
	<div class="conteudo">
		<h1>Veja como fazer parte e como colaborar para transformar este sonho em uma realidade</h1>

		<div id="itens-vejacomo">
			<a href="/para-voce">
				<div class="item-vejacomo soft-hover">
					<img src="img/para-voce-sobre.png" alt="TrocoSimples">

					<h2>Para Você</h2>
				</div>
			</a>
			<a href="/para-empresas">
				<div class="item-vejacomo soft-hover">
					<img src="img/para-empresas-sobre.png" alt="TrocoSimples">

					<h2>Para Empresas</h2>
				</div>
			</a>
		</div>
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->