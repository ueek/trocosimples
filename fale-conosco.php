<section class="banner-internas" style="background: linear-gradient(90deg, rgba(255, 113, 0, 0.3) 24.37%, rgba(255, 255, 255, 0.2) 76.25%), url(/img/bg-faleconosco.png); background-repeat: no-repeat; background-position: center;"></section>

<section class="sep-banner"></section>

<section id="fale-conosco">
	<div class="conteudo">

		<div id="left" class="fale-computador">
			<h1>Fale Conosco</h1>

			<p>Quer falar com a Troco Simples? <br>Baste preencher os campos ao lado e clicar em Enviar formulário.</p>

			<h2>Telefone:</h2>
			<span>+55 41 3514 4930</span>

			<h2>E-mail</h2>
			<span>contato@trocosimples.com.br</span>

			<img src="img/logo-fale.png" alt="TrocoSimples">
		</div>

		<div id="left" class="fale-celular">
			<h1>Fale Conosco</h1>

			<p>Quer falar com a Troco Simples? <br>Baste preencher os campos ao lado e clicar em Enviar formulário.</p>
		</div>

		<div id="right" class="">
			
			<form id="form-fale-conosco">
				<label>Nome:</label>
				<input class="soft-hover" type="text" name="nome" placeholder="">

				<label>E-mail:</label>
				<input class="soft-hover" type="text" name="email" placeholder="">
				
				<label>Telefone:</label>
				<input class="soft-hover" type="text" name="telefone" placeholder="">

				<div class="form-radio">
                    <label>Você é:</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="vocee" id="optionsRadios1" value="1" checked=""> Empresa
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="vocee" id="optionsRadios2" value="2"> Usuário
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="vocee" id="optionsRadios3" value="3"> Sistema
                        </label>
                    </div>
                </div>

				<label>Assunto:</label>
				<textarea class="soft-hover" name="assunto"></textarea>

				<input class="soft-hover" type="submit" value="Enviar Formulário">
			</form>

			<div class="formulario-sucesso">
				<h3><img src="img/sucesso.png" alt="TrocoSimples"> Confirmado!</h3>

				<div>Seu formulário foi submetido com sucesso.</div>
			</div>

			<div class="formulario-erro">
				<h3><img src="img/erro.png" alt="TrocoSimples"> Ops!</h3>

				<div>Identificamos algum problema na submissão. <br>Por favor, tente novamente.</div>
			</div>

		</div>


		<div id="left" class="fale-celular">

			<h2>Telefone:</h2>
			<span>+55 41 3514 4930</span>

			<h2>E-mail</h2>
			<span>contato@trocosimples.com.br</span>

			<img src="img/logo-fale.png" alt="TrocoSimples">
		</div>


		<br>
	</div>
</section>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BK8NR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->