$(document).ready(function(){

   //Cadastrar 
    $("form#cadastrar-revendedor").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="nome"]', form).val().trim() == ""){
            $('input[name="nome"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="cidade"]', form).val().trim() == ""){
            $('input[name="cidade"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="email"]', form).val().trim() == ""){
            $('input[name="email"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }  

        if($('textarea[name="mensagem"]', form).val().trim() == ""){
            $('textarea[name="mensagem"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="sobrenome"]', form).val().trim() == ""){
            $('input[name="sobrenome"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="fone"]', form).val().trim() == ""){
            $('input[name="fone"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("curriculo").files.length == 0) {
            $('input[name="curriculo"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        var valores = new FormData();
        valores.append('nome', $('input[name=nome]').val());
        valores.append('cidade', $('input[name=cidade]').val());
        valores.append('email', $('input[name=email]').val());
        valores.append('mensagem', $('textarea[name=mensagem]').val());
        valores.append('sobrenome', $('input[name=sobrenome]').val());
        valores.append('estado', $('select[name=estado]').val());
        valores.append('fone', $('input[name=fone]').val());
        valores.append('curriculo', $("#curriculo")[0].files[0]);


        $.ajax({
            url: "gestor/dados/revendedor-candidatos/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){
                    //alert(json.msg);
                    //window.location.href="index.php";

                    valores = form.serialize();
                    $.ajax({
                        type     : 'POST',
                        url      : 'submit-contact-revendedor.php',
                        data     : valores,
                        dataType : 'json',
                        success  : function(data){
                            $('input', formVisit).removeAttr("disabled");
                            if(data.status == 1){
                                $(".formulario").fadeOut();
                                $(".formulario-sucesso").fadeIn();

                            }else{
                                $(".formulario").fadeOut();
                                $(".formulario-erro").fadeIn();
                                //alert(data.msg);
                                return false;
                            }
                        },
                        error  : function(data){
                            var response = data.responseText;
                            if (response.includes("Mensagem de contato enviada com sucesso")) {
                                $(".formulario").fadeOut();
                                $(".formulario-sucesso").fadeIn();
                            }else{
                                $(".formulario").fadeOut();
                                $(".formulario-erro").fadeIn();
                            }

                        }
                    });

                }else{
                    //alert(json.msg);

                    $(".formulario").fadeOut();
                    $(".formulario-erro").fadeIn();
                }
            },
            error    : function(retorno){
                //alert("erro");
            }
        });
    });

    $('form#form-revendedores').submit(function(e){ 
        e.preventDefault();
        var formVisit = $(this);

        var txt = $('#nome').val();

        if(txt == ""){
        	var txt = "todos";
        }else{
        	var txt = txt;
        }

        var txt = txt.replace( /\s/g, '-' );

        var estado = $('#estado').val();
        var estado = estado.replace( /\s/g, '-' );

        window.location.href = "/revendedores/"+txt+"/"+estado;

    });

});