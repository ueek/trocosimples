$(document).ready(function(){

	$('form.form-para-empresas').submit(function(e){ 
		e.preventDefault();
		var formVisit = $(this);

		//Validacoes
		if($('input[name="empresa"]', formVisit).val() == ""){
			$('input[name="empresa"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="responsavel"]', formVisit).val() == ""){
			$('input[name="responsavel"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="telefone"]', formVisit).val() == ""){
			$('input[name="telefone"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="cidade"]', formVisit).val() == ""){
			$('input[name="cidade"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="estado"]', formVisit).val() == ""){
			$('input[name="estado"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		var email = $('input[name="email"]', formVisit).val();
		if(email == "" || !checkMail(email)){
			$('input[name="email"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
        
		//Ajax
		var valores = formVisit.serialize();
		$('input', formVisit).attr("disabled", "disabled");
		$.ajax({
			type     : 'POST',
			url      : 'php/envia-para-empresas.php',
			data     : valores,
			dataType : 'json',
			success  : function(data){
				$('input', formVisit).removeAttr("disabled");
				if(data.status == 1){
                    //alert(data.msg);
                    //window.location.reload();


                    $.ajax({
						type     : 'POST',
						url      : 'submit-contact-empresa.php',
						data     : valores,
						dataType : 'json',
						success  : function(data){
							$('input', formVisit).removeAttr("disabled");
							if(data.status == 1){
			                    //alert(data.msg);
			                    //window.location.reload();
			                    window.location.href = "/para-empresas-agradecimento";

			                    /*$(".formulario").fadeOut();
			                    $(".formulario-sucesso").fadeIn();*/

							}else{
								$(".formulario").fadeOut();
			                    $(".formulario-erro").fadeIn();
			                    //alert(data.msg);
			                    return false;
							}
						},
						error  : function(data){
							var response = data.responseText;
							if (response.includes("Mensagem de contato enviada com sucesso")) {
			                    /*$(".formulario").fadeOut();
			                    $(".formulario-sucesso").fadeIn();*/

			                    window.location.href = "/para-empresas-agradecimento";

							}else{
								$(".formulario").fadeOut();
			                    $(".formulario-erro").fadeIn();
							}

						}
					});

				}else{
					$(".formulario").fadeOut();
                    $(".formulario-erro").fadeIn();

                    //alert(data.msg);
                    return false;
				}
			}
		});
	});



	$('form#form-fale-conosco').submit(function(e){ 
		e.preventDefault();
		var formVisit = $(this);

		//Validacoes
		if($('input[name="nome"]', formVisit).val() == ""){
			$('input[name="nome"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		var email = $('input[name="email"]', formVisit).val();
		if(email == "" || !checkMail(email)){
			$('input[name="email"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="telefone"]', formVisit).val() == ""){
			$('input[name="telefone"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="assunto"]', formVisit).val() == ""){
			$('input[name="assunto"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

        
		//Ajax
		var valores = formVisit.serialize();
		$('input', formVisit).attr("disabled", "disabled");
		$.ajax({
			type     : 'POST',
			url      : 'php/envia-fale-conosco.php',
			data     : valores,
			dataType : 'json',
			success  : function(data){
				$('input', formVisit).removeAttr("disabled");
				if(data.status == 1){

                    $.ajax({
						type     : 'POST',
						url      : 'submit-contact-fale-conosco.php',
						data     : valores,
						dataType : 'json',
						success  : function(data){
							$('input', formVisit).removeAttr("disabled");
							if(data.status == 1){
			                    $("#form-fale-conosco").fadeOut();
			                    $(".formulario-sucesso").fadeIn();

							}else{
								$("#form-fale-conosco").fadeOut();
			                    $(".formulario-erro").fadeIn();
			                    //alert(data.msg);
			                    return false;
							}
						},
						error  : function(data){
							var response = data.responseText;
							if (response.includes("Mensagem de contato enviada com sucesso")) {
			                    $("#form-fale-conosco").fadeOut();
			                    $(".formulario-sucesso").fadeIn();
							}else{
								$("#form-fale-conosco").fadeOut();
			                    $(".formulario-erro").fadeIn();
							}

						}
					});					

				}else{
					$("#form-fale-conosco").fadeOut();
                    $(".formulario-erro").fadeIn();

                    //alert(data.msg);
                    return false;
				}
			}
		});
	});

	$('form#form-sistema').submit(function(e){ 
		e.preventDefault();
		var formVisit = $(this);

		//Validacoes
		if($('input[name="nome"]', formVisit).val() == ""){
			$('input[name="nome"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		var email = $('input[name="email"]', formVisit).val();
		if(email == "" || !checkMail(email)){
			$('input[name="email"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="cidade"]', formVisit).val() == ""){
			$('input[name="cidade"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		if($('input[name="telefone"]', formVisit).val() == ""){
			$('input[name="telefone"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

        
		//Ajax
		var valores = formVisit.serialize();
		$('input', formVisit).attr("disabled", "disabled");
		$.ajax({
			type     : 'POST',
			url      : 'php/envia-sistema.php',
			data     : valores,
			dataType : 'json',
			success  : function(data){
				$('input', formVisit).removeAttr("disabled");
				if(data.status == 1){

                    $.ajax({
						type     : 'POST',
						url      : 'submit-contact-sistema.php',
						data     : valores,
						dataType : 'json',
						success  : function(data){
							$('input', formVisit).removeAttr("disabled");
							if(data.status == 1){
			                    $("#form-sistema").fadeOut();
			                    $(".formulario-sucesso").fadeIn();

							}else{
								$("#form-fale-conosco").fadeOut();
			                    $(".formulario-erro").fadeIn();
			                    //alert(data.msg);
			                    return false;
							}
						},
						error  : function(data){
							var response = data.responseText;
							if (response.includes("Mensagem de contato enviada com sucesso")) {
			                    $("#form-sistema").fadeOut();
			                    $(".formulario-sucesso").fadeIn();
							}else{
								$("#form-sistema").fadeOut();
			                    $(".formulario-erro").fadeIn();
							}

						}
					});					

				}else{
					$("#form-sistema").fadeOut();
                    $(".formulario-erro").fadeIn();

                    //alert(data.msg);
                    return false;
				}
			}
		});
	});

	$('form#form-newsletter').submit(function(e){ 
		e.preventDefault();
		var formVisit = $(this);

		//Validacoes
		if($('input[name="nome"]', formVisit).val() == ""){
			$('input[name="nome"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}

		var email = $('input[name="email"]', formVisit).val();
		if(email == "" || !checkMail(email)){
			$('input[name="email"]', formVisit).css('border-color', '#be0007').focus();
			return false;
		}
        
		//Ajax
		var valores = formVisit.serialize();
		$('input', formVisit).attr("disabled", "disabled");
		$.ajax({
			type     : 'POST',
			url      : 'submit-contact-newsletter.php',
			data     : valores,
			dataType : 'json',
			success  : function(data){
				$('input', formVisit).removeAttr("disabled");
				if(data.status == 1){
                    //alert(data.msg);
                    //window.location.reload();

                    $("#form-newsletter").fadeOut();
                    $(".formulario-sucesso").fadeIn();

				}else{
					$("#form-newsletter").fadeOut();
                    $(".formulario-erro").fadeIn();

                    //alert(data.msg);
                    return false;
				}
			},
			error  : function(data){
				var response = data.responseText;
				if (response.includes("Mensagem de contato enviada com sucesso")) {
                    $("#form-newsletter").fadeOut();
                    $(".formulario-sucesso").fadeIn();
				}else{
					$("#form-newsletter").fadeOut();
                    $(".formulario-erro").fadeIn();
				}

			}
		});
	});
});