$(document).on('ready', function() {
	
	$('.regular5').slick({
		dots: true,
		infinite: false,
		speed: 600,
		slidesToShow: 5,
		slidesToScroll: 5,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [
		{
		  breakpoint: 1024,
		  settings: {
		    slidesToShow: 5,
		    slidesToScroll: 5
		  }
		},
		{
		  breakpoint: 680,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	$('.regular4').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: false,
		autoplaySpeed: 2000,
		responsive: [
		{
		  breakpoint: 1024,
		  settings: {
		    slidesToShow: 3,
		    slidesToScroll: 3
		  }
		},
		{
		  breakpoint: 680,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});


	$('.regular3').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
	    {
			breakpoint: 1024,
			settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
  			}
	    },
	    {
	      breakpoint: 680,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});


	$('.regular2').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			dots: true
	  		}
		},
		{
		  breakpoint: 680,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});


	$('.regular1').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 680,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});
});

$(document).ready(function(){

	$(".btn-formularios").click(function(){
		$(".btn-formularios").fadeOut();
		$(".formulario").fadeIn();
	});

	/***** Menu fixo *****/
	topo = $('.sep-banner').offset().top;

	if(topo > 1080){
		topo = 1080;
	}

	var $i = $(window);

	$i.on("scroll", function(){
	   if( $i.scrollTop() > topo) {
	   		
       		//$('.topo').css('border', '1px solid red');

       		$('.header-fixo').fadeIn(300);
	   }else{
   			$('.header-fixo').fadeOut(300);
	   }
	});
	/***** Menu fixo *****/


	/***** Itens - Sobre nós *****/
	$(".item-trocosimples").click(function(){

		var display = $(".txt-trocosimples", this).css('display');

		$(".txt-trocosimples").css("display", "none");

		if(display == "none"){
			$(".txt-trocosimples", this).css("display", "block");
		}else if(display == "block"){
			$(".txt-trocosimples", this).css("display", "none");
		}

	});
	/***** Itens - Sobre nós *****/



	//ScrollReveal
	window.sr = ScrollReveal({ reset: true, distance: '80px' });
	sr.reveal('.efeito');

	//Custom Settings
	sr.reveal('.fadeIn', { duration: 1000 });

	sr.reveal('.rightShow', { 
	  origin: 'right', 
	  duration: 1000 
	});

	sr.reveal('.leftShow', { 
	  origin: 'left', 
	  duration: 1000 
	});

	sr.reveal('.topShow', { 
	  origin: 'top', 
	  duration: 1000 
	});



	//Banner 100%
	var j = $(window).height();
	$('.topo-home').css("height", j);

	$(window).resize(function(){
		j = $(window).height();
       	$('.topo-home').css("height", j);
    });


    //Menu 100%
	var j = $(window).height();
	$('#menu-cel').css("height", j);

	$(window).resize(function(){
		j = $(window).height();
       	$('#menu-cel').css("height", j);
    });



	/*$("body").click(function(e) {
		var largura = $(window).width();

		alert(largura);
	});*/



	//Rolagem
	function filterPath(string) {
		return string
		.replace(/^\//,'')
		.replace(/(index|default).[a-zA-Z]{3,4}$/,'')
		.replace(/\/$/,'');
	}

	$('a[href*=#]').each(function() {
		if ( filterPath(location.pathname) == filterPath(this.pathname)
		&& location.hostname == this.hostname
		&& this.hash.replace(/#/,'') ) {
		var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
		var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;

		if ($target) {
			var targetOffset = $target.offset().top;
			$(this).click(function() {
				$('html, body').animate({scrollTop: targetOffset}, 300);
				return false;
				});
			}
		}
	});
	
});
